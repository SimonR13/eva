package da.lecture.simon.chat;

import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;

/**
 * Simon-Schnittstelle für ein Simon-Objekt, welches einen Chatter repräsentiert, der über ein {@link ChatRoom}-Objekte mit
 * anderen {@link Chatter}n kommunizieren will.
 */
@SuppressWarnings("deprecation")
public interface Chatter extends SimonRemote {
    /**
     * Muss den Namen des Chatters zurückliefern.
     * @return Name, der innerhalb des ChatRooms verwendet werden soll.
     * @throws SimonRemoteException Wird ausgelöst, wenn es bei der Simon-Kommunikation zu einem Fehler gekommen ist.
     */
    String getName() throws SimonRemoteException;

    /**
     * Wird vom {@link ChatRoom} zur Zustellung von neuen Nachrichten aufgerufen.
     * @param message Neue Nachricht.
     * @throws SimonRemoteException Wird ausgelöst, wenn es bei der Simon-Kommunikation zu einem Fehler gekommen ist.
     */
    void printMessage(String message) throws SimonRemoteException;
}
