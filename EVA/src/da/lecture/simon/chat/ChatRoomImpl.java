package da.lecture.simon.chat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.root1.simon.exceptions.SimonRemoteException;

/**
 * Implementierung von {@link ChatRoom}, die einen über Simon operierenden Chat-Raum nach dem Entwurfsmuster Oberserver zur
 * Verfügung stellt.
 */
public class ChatRoomImpl implements ChatRoom {
    /** Liste aller Benutzer dieses Chat-Raumes. */
    private final List<Chatter> chatters;

    /**
     * Initialisiert eine neue ChatRoomImp-Instanz.
     */
    public ChatRoomImpl() {
        this.chatters = new ArrayList<>();
    }

    @Override public synchronized void addChatter(final Chatter newChatter) throws ChatterWithNameAlreadyAddedException {
        final String newChatterName = newChatter.getName();
        final Iterator<Chatter> chatterIter = this.chatters.iterator();

        Chatter currentChatter = null;
        while (chatterIter.hasNext()) {
            currentChatter = chatterIter.next();

            try {
                if (currentChatter.getName().equals(newChatterName)) {
                    throw new ChatterWithNameAlreadyAddedException();
                }
            } catch (final SimonRemoteException re) {
                // Es scheint ein Kommunikationsproblem mit dem gerade betrachteten Chatter zu geben. Es kann davon ausgegangen
                // werden, dass die Verbindung zu diesem Chatter gestört ist. Daher wird er aus der Liste der Benutzer dieses
                // Chat-Raumes entfernt.
                chatterIter.remove();
            }
        }

        this.chatters.add(newChatter);
    }

    @Override public synchronized void removeChatter(final Chatter chatter) {
        this.chatters.remove(chatter);
    }

    @Override public synchronized void sendMessage(final String name, final String message) {
        final String messageToPrint = name + ": " + message;
        final Iterator<Chatter> chatterIter = this.chatters.iterator();

        Chatter currentChatter = null;
        while (chatterIter.hasNext()) {
            currentChatter = chatterIter.next();

            try {
                currentChatter.printMessage(messageToPrint);
            } catch (final SimonRemoteException re) {
                // Es scheint ein Kommunikationsproblem mit dem gerade betrachteten Chatter zu geben. Es kann davon ausgegangen
                // werden, dass die Verbindung zu diesem Chatter gestört ist. Daher wird er aus der Liste der Benutzer dieses
                // Chat-Raumes entfernt.
                chatterIter.remove();
            }
        }
    }
}
