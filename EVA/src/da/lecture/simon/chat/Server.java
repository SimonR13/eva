package da.lecture.simon.chat;

import java.io.IOException;
import java.net.UnknownHostException;

import de.root1.simon.Registry;
import de.root1.simon.Simon;
import de.root1.simon.exceptions.NameBindingException;

/**
 * Server-Klasse, die zuerst eine lokale Simon-Registry startet und dann ein Objekt vom Typ {@link ChatRoomImpl} bei dieser unter
 * dem Namen {@link ChatRoom#DEFAULT_SIMON_OBJECT_NAME} registriert.
 */
public class Server {
    /** Standard-Port für die zu startende Simon-Registry. */
    public static final int REGISTRY_PORT = 22222;

    /**
     * Hauptprogramm.
     * @param args Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws UnknownHostException
     * @throws IOException Wird ausgelöst, wenn es bei der Erzeugung oder Kommunikation mit der Simon-Registry zu einem
     *             Ein-/Ausgabefehler gekommen ist.
     * @throws NameBindingException Wird ausgelöst, wenn unter dem Namen {@link ChatRoom#DEFAULT_SIMON_OBJECT_NAME} bereits ein
     *             anderes Simon-Objekt bei der RMI-Registry registriert ist.
     */
    public static void main(final String[] args) throws NameBindingException, IOException {
        final ChatRoom room = new ChatRoomImpl();
        final Registry localSimonReg = Simon.createRegistry(Server.REGISTRY_PORT);
        localSimonReg.bind(ChatRoom.DEFAULT_SIMON_OBJECT_NAME, room);
        System.out.println("Chat-Server bereit");
    }
}
