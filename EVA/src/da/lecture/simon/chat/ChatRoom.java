package da.lecture.simon.chat;

import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;

/**
 * Simon-Schnittstelle für ein Simon-Objekt, welches einen Chat-Raum repräsentiert, über den {@link Chatter}-Objekte miteinaner
 * chatten können.
 */
@SuppressWarnings("deprecation")
public interface ChatRoom extends SimonRemote {
    /** Konstante für den Standardnamen eines RemoteAppender-Objektes in einer Simon-Registry. */
    String DEFAULT_SIMON_OBJECT_NAME = "ChatRoom";

    /**
     * Meldet den übergebenen {@link Chatter} an, so dass ihm in Zukunft die über diesen {@link ChatRoom} ausgetauschten
     * Nachrichten über {@link Chatter#printMessage(String)} zugestellt werden.
     * @param chatter Anzumeldender Chatter.
     * @throws SimonRemoteException Wird ausgelöst, wenn es bei der Simon-Kommunikation zu einem Fehler gekommen ist.
     * @throws ChatterWithNameAlreadyAddedException Wird ausgelöst, falls in diesem {@link ChatRoom} bereits ein anderer
     *             {@link Chatter} angemeldet ist, der den gleichen Namen verwendet.
     */
    void addChatter(Chatter chatter) throws SimonRemoteException, ChatterWithNameAlreadyAddedException;

    /**
     * Meldet den übergebenen {@link Chatter} ab, so dass ihm in Zukunft keine über diesen {@link ChatRoom} ausgetauschten
     * Nachrichten mehr über {@link Chatter#printMessage(String)} zugestellt werden.
     * @param chatter Abzumeldender Chatter.
     * @throws SimonRemoteException Wird ausgelöst, wenn es bei der Simon-Kommunikation zu einem Fehler gekommen ist.
     */
    void removeChatter(Chatter chatter) throws SimonRemoteException;

    /**
     * Sendet die übergebene Nachricht mit dem übergebenen Namen an alle in diesem {@link ChatRoom} angemeldeten {@link Chatter}.
     * @param name Name, von dem die Nachricht kommt.
     * @param message Nachricht, die an alle angemeldeten {@link Chatter} übermittelt werden soll.
     * @throws SimonRemoteException Wird ausgelöst, wenn es bei der Simon-Kommunikation zu einem Fehler gekommen ist.
     */
    void sendMessage(String name, String message) throws SimonRemoteException;

    //
    //

    /**
     * Ausnahme, die von der Methode {@link ChatRoom#addChatter(Chatter)} immer dann ausgelöst wird, wenn ein {@link Chatter} mit
     * einem Namen angemeldet werden soll, der bereits von einem anderen {@link Chatter} des {@link ChatRoom}s verwendet wird.
     */
    public class ChatterWithNameAlreadyAddedException extends Exception {
        // Keine erweiternde Implementierung notwendig.
    }
}
