package da.lecture.simon.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.rmi.RemoteException;

import da.lecture.simon.chat.ChatRoom.ChatterWithNameAlreadyAddedException;
import de.root1.simon.Lookup;
import de.root1.simon.Simon;
import de.root1.simon.exceptions.EstablishConnectionFailed;
import de.root1.simon.exceptions.LookupFailedException;

/**
 * Implementierung der Schnittestelle {@link Chatter} zum Austauschen von Meldungen mit anderen {@link Chatter}n in einem
 * {@link ChatRoom}.
 */
public class CommandLineClient implements Chatter {
    /** Name des Chatters. */
    private final String name;
    /** {@link ChatRoom}, in dem der Chatter angemeldet ist. */
    private final ChatRoom chatRoom;

    /**
     * Initialisiert eine neue CommandLineClient-Instanz mit den übergebenen Argumenten.
     * @param name Gewünschter Name.
     * @param chatRoom {@link ChatRoom}, über den kommuniziert werden soll.
     * @throws ChatterWithNameAlreadyAddedException Wird ausgelöst, wenn ein bereits beim übergebenen {@link ChatRoom}
     *             angemeldeter {@link Chatter} den gleichen Namen verwendet.
     */
    public CommandLineClient(final String name, final ChatRoom chatRoom) throws ChatterWithNameAlreadyAddedException {
        this.name = name;
        this.chatRoom = chatRoom;
        this.chatRoom.addChatter(this);
    }

    @Override public String getName() {
        return this.name;
    }

    @Override public void printMessage(final String message) {
        System.out.println(message);
    }

    /**
     * Hilfsmethode, die Zeilenweise von der Konsole liest und diese Zeilen an den {@link ChatRoom} im Attribut {@link #chatRoom}
     * weitergibt.
     */
    public void sendConsoleMessagesToChatRoom() {
        try (final BufferedReader consoleIn = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String readLine = null;
                while ((readLine = consoleIn.readLine()) != null) {
                    if (readLine.equals("exit")) {
                        break;
                    }

                    this.chatRoom.sendMessage(this.name, readLine);
                }

            }
        } catch (final RemoteException re) {
            System.err.println("Kommunikationsfehler mit Chat-Raum: " + re.getMessage());
        } catch (final IOException ioe) {
            System.err.println("Ein-/Ausgabefehler auf Kommandozeile: " + ioe.getMessage());
        }
    }

    //
    //

    /**
     * Hauptprogramm. Als Kommandozeilenargumente müssen die Adresse des Rechners angegeben werden, auf dem die RMI-Registry
     * läuft, sowie der Name, der für den Chat verwendet werden soll.
     * @param args Kommandozeilenargumente.
     * @throws UnknownHostException Wird ausgelöst, wenn der Rechner, auf dem angeblich die Simon-Registry laufen soll, unbekannt
     *             ist.
     * @throws EstablishConnectionFailed Wird ausgelöst, wenn es beim Verbindungsaufbau mit der Simon-Registry zu einem Fehler
     *             gekommen ist.
     * @throws LookupFailedException Wird ausgelöst, wenn bei der Simon-Registry auf dem Rechner mit der übergebenen Adresse kein
     *             Objekt mit dem Namen {@link ChatRoom#DEFAULT_SIMON_OBJECT_NAME} registriert ist.
     */
    public static void main(final String[] args) throws LookupFailedException, EstablishConnectionFailed, UnknownHostException {
        final Lookup remoteObjLookup = Simon.createNameLookup(args[0], Server.REGISTRY_PORT);
        final ChatRoom room = (ChatRoom) remoteObjLookup.lookup(ChatRoom.DEFAULT_SIMON_OBJECT_NAME);
        final String name = args[1];

        try {
            final CommandLineClient cmdLineClient = new CommandLineClient(name, room);
            cmdLineClient.sendConsoleMessagesToChatRoom();
            room.removeChatter(cmdLineClient);

        } catch (final ChatterWithNameAlreadyAddedException e) {
            System.err.println("Ein Benutzer mit diesem Namen ist bereits anwesend!");
        } finally {
            remoteObjLookup.release(room);
            // System.exit(0);
        }
    }
}
