package da.lecture.io.channel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;

/**
 * Klasse zur Messung der benötigten Zeit zum zeilenweise einlesen einer Datei,
 * deren Name als Kommandozeilenargument angegeben werden muss.
 */
public class NIOPerformanceTest
{
    /**
     * Hauptprogramm. Der Pfad zu der zur Messung zu verwendenden Datei muss als
     * Kommandozeilenargument angegeben werden.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Einlesen zu einem Ein- /
     *             Ausgabe-Fehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final File f = new File(args[0]);

        System.out.println("IO: " + NIOPerformanceTest.readLinesWithIO(f));
        System.out.println("NIO: " + NIOPerformanceTest.readLinesWithNIO(f));
        System.out.println("NIO2IO: " + NIOPerformanceTest.readLinesWithNIO2IOBridge(f));
        System.out.println("NIO mit Liste: " + NIOPerformanceTest.readLinesWithNIOUsingList(f));
    }

    //
    //

    /**
     * Zeilenweises Einlesen der übergebenen Datei unter Verwendung eines
     * "normalen" {@link BufferedReader}s.
     * 
     * @param f
     *            Zeilenweise einzulesende Datei.
     * @return Laufzeit in Millisekunden.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Einlesen zu einem Ein- /
     *             Ausgabe-Fehler gekommen ist.
     */
    public static long readLinesWithIO(final File f) throws IOException
    {
        final long start = System.currentTimeMillis();

        try (final BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(f), "US-ASCII")))
        {
            @SuppressWarnings("unused")
            String line;
            while ((line = r.readLine()) != null)
            { // Blockiert bis komplette Zeile gelesen ist
                // Irgendeine Verarbeitung der Zeile
            }
        }
        return System.currentTimeMillis() - start;
    }

    /**
     * Zeilenweises Einlesen der übergebenen Datei unter Verwendung eines
     * {@link FileChannel}s.
     * 
     * @param f
     *            Zeilenweise einzulesende Datei.
     * @return Laufzeit in Millisekunden.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Einlesen zu einem Ein- /
     *             Ausgabe-Fehler gekommen ist.
     */
    public static long readLinesWithNIO(final File f) throws IOException
    {
        final long start = System.currentTimeMillis();

        final ByteBuffer bBuf = ByteBuffer.allocate(8192); // Byte-Puffer zum
                                                           // Lesen aus dem
                                                           // Kanal
        final StringBuilder lineBuffer = new StringBuilder(); // Zeilen-Puffer
                                                              // zum Sammeln
                                                              // einer Zeile
        try (@SuppressWarnings("resource")
        final FileChannel ch = new FileInputStream(f).getChannel())
        {
            while (ch.read(bBuf) != -1)
            { // So lange lesen, bis Dateiende erreicht
                bBuf.flip(); // Byte-Puffer in den Lese-Modus versetzen
                final CharBuffer cBuf = Charset.forName("US-ASCII").decode(bBuf);
                bBuf.clear(); // Byte-Puffer für nächstens Schreiben leeren, in
                              // Schreib-Modus versetzen

                for (int i = 0; i < cBuf.length(); i++)
                { // Alle Zeichen des Zeichen-Puffers durchlaufen
                    final char currentChar = cBuf.get(i); // Nächsten, noch
                                                          // nicht betrachteten
                                                          // Buchstaben beziehen
                    if (currentChar != '\n')
                    { // Falls kein Zeilenumbruch, dann Zeilen-Puffer weiter
                      // füllen
                        lineBuffer.append(currentChar);

                    }
                    else
                    { // Falls Zeilenumbruch, dann enthält Zeilen-Puffer
                      // vollständige Zeile
                        @SuppressWarnings("unused")
                        final String line = lineBuffer.toString(); // Zeilen-Puffer
                                                                   // zu
                                                                   // Zeichenkette
                        // Irgendeine Verarbeitung der Zeile

                        lineBuffer.setLength(0); // Zeilen-Puffer für nächste
                                                 // Zeile leeren
                    }
                }
            }
        }
        return System.currentTimeMillis() - start;
    }

    /**
     * Zeilenweises Einlesen der übergebenen Datei unter Verwendung eines
     * {@link BufferedReader}s, der auf einem {@link FileChannel} aufsetzt
     * (siehe
     * {@link Channels#newReader(java.nio.channels.ReadableByteChannel, java.nio.charset.CharsetDecoder, String)}
     * ).
     * 
     * @param f
     *            Zeilenweise einzulesende Datei.
     * @return Laufzeit in Millisekunden.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Einlesen zu einem Ein- /
     *             Ausgabe-Fehler gekommen ist.
     */
    public static long readLinesWithNIO2IOBridge(final File f) throws IOException
    {
        final long start = System.currentTimeMillis();

        try (@SuppressWarnings("resource")
        final FileChannel ch = new FileInputStream(f).getChannel();
        final BufferedReader r = new BufferedReader(Channels.newReader(ch, "US-ASCII")))
        {
            @SuppressWarnings("unused")
            String line;
            while ((line = r.readLine()) != null)
            { // Blockiert bis komplette Zeile gelesen ist
                // Irgendeine Verarbeitung der Zeile
            }
        }
        return System.currentTimeMillis() - start;
    }

    /**
     * Zeilenweises Einlesen der übergebenen Datei in eine Liste unter
     * Verwendung der Methode
     * {@link Files#readAllLines(java.nio.file.Path, Charset)}.
     * 
     * @param f
     *            Zeilenweise einzulesende Datei.
     * @return Laufzeit in Millisekunden.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Einlesen zu einem Ein- /
     *             Ausgabe-Fehler gekommen ist.
     */
    public static long readLinesWithNIOUsingList(final File f) throws IOException
    {
        final long start = System.currentTimeMillis();

        final List<String> allLines = Files.readAllLines(f.toPath(), Charset.forName("US-ASCII"));
        for (@SuppressWarnings("unused")
        final String line : allLines)
        {
            // Irgendeine Verarbeitung der Zeile
        }
        return System.currentTimeMillis() - start;
    }
}
