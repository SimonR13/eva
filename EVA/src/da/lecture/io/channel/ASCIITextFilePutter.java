package da.lecture.io.channel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * Einfache Klasse zum Schreiben einer ASCII-Textdatei.
 */
public class ASCIITextFilePutter
{
    /**
     * Hauptprogramm. Kommandozeilenargumente werden in die Textdatei t.txt
     * geschrieben.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Schreiben zu einem Ein- /
     *             Ausgabe-Fehler gekommen ist.
     */
    @SuppressWarnings("resource")
    public static void main(final String[] args) throws IOException
    {
        final CharBuffer cBuf = CharBuffer.allocate(1024); // Datenpuffer mit
                                                           // fester Größe
        for (final String currentString : args)
        {
            cBuf.append(currentString); // Datenpuffer könnte zu klein sein!
        }
        cBuf.flip(); // Umschalten von Schreib- in Lesemodus

        final ByteBuffer bBuf = Charset.forName("US-ASCII").encode(cBuf); // Umwandlung
                                                                          // in
                                                                          // bytes
        try (final FileChannel ch = new FileOutputStream("t.txt").getChannel())
        {
            ch.write(bBuf);
        }
    }
}
