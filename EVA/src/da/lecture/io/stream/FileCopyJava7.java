package da.lecture.io.stream;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Einfache Klasse zum Kopieren der Inhalte einer Datei in eine andere, wobei
 * das mit Java 7 eingef�hrte automatische Resourcenmanagement verwendet wird.
 */
public class FileCopyJava7
{
    /**
     * Hauptprogramm. Die Pfade zu den beiden Dateien m�ssen als
     * Kommandozeilenargumente angegeben werden.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgel�st, wenn es beim Kopieren zu einem Ein- /
     *             Ausgabe-Fehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final FileInputStream is = new FileInputStream(args[0]);
        final FileOutputStream os = new FileOutputStream(args[1]))
        {

            int data = 0;
            while ((data = is.read()) != -1)
            {
                os.write(data);
            }
        }
    }
}
