package da.lecture.io.stream;

import java.io.BufferedReader;
import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Filterstrom zum zeilenweise Lesen von einem beliebigen Zeichentrom, wobei nur
 * solche Zeilen zurückgeliefert werden, die eine bestimmte Zeichenkette
 * beinhalten.
 */
public class GrepReader extends FilterReader
{
    /** Zeichenkette, die enthalten sein soll. */
    private final String substring;

    /**
     * Initialisiert eine neue GrepReader-Instanz mit den übergebenen
     * Argumenten.
     * 
     * @param in
     *            Zeichenstrom, von dem zeilenweise gelesen werden soll.
     * @param substring
     *            Zeichenkette, die in gelesenen Zeilen enthalten sein muss,
     *            damit sie auch zurückgeliefert werden.
     */
    public GrepReader(final Reader in, final String substring)
    {
        super(new BufferedReader(in));
        this.substring = substring;
    }

    /**
     * Liest zeilenweise vom über den Konstruktor übergebenen Zeichenstrom und
     * liefert die nächste Zeile zurück, die die über den Konstruktor übergebene
     * Zeichenektte enthält.
     * 
     * @return Siehe Methodenbeschreibung.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Lesen vom über den Konstruktor
     *             übergebenen Zeichenstrom zu einem Ein-/Ausgabefehler gekommen
     *             ist.
     */
    public String readLine() throws IOException
    {
        String line;
        do
        {
            line = ((BufferedReader) super.in).readLine();
        }
        while ((line != null) && !line.contains(this.substring));

        return line;
    }
}
