package da.lecture.io.stream;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Einfache Klasse zum Kopieren der Inhalte einer Datei in eine andere.
 */
public class FileCopy
{
    /**
     * Hauptprogramm. Die Pfade zu den beiden Dateien müssen als
     * Kommandozeilenargumente angegeben werden.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Kopieren zu einem Ein- /
     *             Ausgabe-Fehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        FileInputStream is = null;
        FileOutputStream os = null;

        try
        {
            is = new FileInputStream(args[0]);
            os = new FileOutputStream(args[1]);

            int data = 0;
            while ((data = is.read()) != -1)
            {
                os.write(data);
            }
        }
        finally
        {
            if (is != null)
            {
                try
                {
                    is.close();
                }
                catch (final IOException e)
                {
                    e.printStackTrace();
                }
            }
            if (os != null)
            {
                try
                {
                    os.close();
                }
                catch (final IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
