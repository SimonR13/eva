package da.lecture.io.stream.serialization;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Klasse zur Veranschaulichung der Serialisierung eines Objektes vom Typ
 * {@link Point} über {@link ObjectOutputStream} und {@link ObjectInputStream}.
 */
public class ObjectStreamSerialization
{
    /** Name der Datei, die für die Serialisierung verwendet werden soll. */
    public static final String FNAME = "seriTest.binary";

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws Exception
     *             Wird ausgelöst, wenn es bei Serialisierung bzw.
     *             Deserialisierung zu Problemen gekommen ist.
     */
    public static void main(final String[] args) throws Exception
    {
        final Point origPoint = new Point(30, 30);
        ObjectStreamSerialization.writePoint(origPoint);
        final Point restoredPoint = ObjectStreamSerialization.readPoint();
        System.out.println(restoredPoint.toString());
    }

    /**
     * Hilfsmethode, die das übergebene {@link Point}-Objekt in die Datei mit
     * dem Namen {@link DataStreamSerialization#FNAME} speichert.
     * 
     * @param p
     *            Zu serialisierendes {@link Point}-Objekt.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Serialisieren zu einem
     *             Ein-/Ausgabefehler gekommen ist.
     */
    private static void writePoint(final Point p) throws IOException
    {
        try (final ObjectOutputStream oOut = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(DataStreamSerialization.FNAME))))
        {
            oOut.writeObject(p);
        }
    }

    /**
     * Hilfsmtehode, die ein {@link Point}-Objekt aus der Datei
     * {@link DataStreamSerialization#FNAME} deserialisiert und zurückliefert.
     * 
     * @return Deserialisiertes {@link Point}-Objekt.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Deserialisieren zu einem
     *             Ein-/Ausgabefehler gekommen ist.
     * @throws ClassNotFoundException
     *             Wird ausgelöst, wenn das deserialisierte Objekt vom Typ einer
     *             Klasse ist, die nicht gefunden werden konnte.
     */
    private static Point readPoint() throws IOException, ClassNotFoundException
    {
        try (final ObjectInputStream oIn = new ObjectInputStream(new BufferedInputStream(new FileInputStream(DataStreamSerialization.FNAME))))
        {
            return (Point) oIn.readObject();
        }
    }
}
