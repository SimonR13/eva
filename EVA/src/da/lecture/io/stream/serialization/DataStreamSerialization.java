package da.lecture.io.stream.serialization;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Klasse zur Veranschaulichung der Serialisierung eines Objektes vom Typ
 * {@link Point} über {@link DataOutputStream} und {@link DataInputStream}.
 */
public class DataStreamSerialization
{
    /** Name der Datei, die für die Serialisierung verwendet werden soll. */
    public static final String FNAME = "seriTest.binary";

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Kopieren zu einem Ein- /
     *             Ausgabe-Fehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final Point origPoint = new Point(30, 30);
        DataStreamSerialization.writePoint(origPoint);
        final Point restoredPoint = DataStreamSerialization.readPoint();
        System.out.println(restoredPoint.toString());
    }

    /**
     * Hilfsmethode, die das übergebene {@link Point}-Objekt in die Datei mit
     * dem Namen {@link DataStreamSerialization#FNAME} speichert.
     * 
     * @param p
     *            Zu serialisierendes {@link Point}-Objekt.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Serialisieren zu einem
     *             Ein-/Ausgabefehler gekommen ist.
     */
    private static void writePoint(final Point p) throws IOException
    {
        try (final DataOutputStream dOut = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(DataStreamSerialization.FNAME))))
        {
            dOut.writeInt(p.getX());
            dOut.writeInt(p.getY());
        }
    }

    /**
     * Hilfsmtehode, die ein {@link Point}-Objekt aus der Datei
     * {@link DataStreamSerialization#FNAME} deserialisiert und zurückliefert.
     * 
     * @return Deserialisiertes {@link Point}-Objekt.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Deserialisieren zu einem
     *             Ein-/Ausgabefehler gekommen ist.
     */
    private static Point readPoint() throws IOException
    {
        try (final DataInputStream dIn = new DataInputStream(new BufferedInputStream(new FileInputStream(DataStreamSerialization.FNAME))))
        {
            final int x = dIn.readInt();
            final int y = dIn.readInt();

            return new Point(x, y);
        }
    }
}
