package da.lecture.io.stream;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

/**
 * Einfache Klasse zum Dekomprimieren der Inhalte einer mit GZIP komprimierten
 * Datei in eine andere.
 */
public class GZIPExtractor
{
    /**
     * Hauptprogramm. Die Pfade zu den beiden Dateien m�ssen als
     * Kommandozeilenargumente angegeben werden.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgel�st, wenn es beim Kopieren zu einem Ein- /
     *             Ausgabe-Fehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final GZIPInputStream gis = new GZIPInputStream(new FileInputStream(args[0]));
        final FileOutputStream os = new FileOutputStream(args[1]))
        {

            int uncompressedData = 0;
            while ((uncompressedData = gis.read()) != -1)
            {
                os.write(uncompressedData);
            }
        }
    }
}
