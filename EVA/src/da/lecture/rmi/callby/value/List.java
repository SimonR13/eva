package da.lecture.rmi.callby.value;

import java.io.Serializable;

/**
 * Einfach verkettete Liste, die aus {@link ListItem}-Objekten aufgebaut ist.
 */
public class List implements Serializable {
    /** Erstes Kettenglied. */
    private ListItem first;
    /** Letztes Kettenglied. */
    private ListItem last;

    /**
     * Initialisiert eine neue List-Instanz mit den übergebenen Argumenten.
     * @param intialValues Werte, die sich initial in der Liste befinden sollen.
     */
    public List(final int... intialValues) {
        for (final int currentValue : intialValues) {
            this.append(currentValue);
        }
    }

    /**
     * Hängt ein neues Kettenglied mit dem übergebenen Wert an das Ende der Liste an.
     * @param valueToAppend Wert, der als neues Kettenglied am Ende der Liste angehängt werden soll.
     */
    public void append(final int valueToAppend) {
        final ListItem itemToAppend = new ListItem(valueToAppend);

        if (this.first == null) {
            this.first = itemToAppend;
        } else {
            this.last.setNext(itemToAppend);
        }
        this.last = itemToAppend;
    }

    @Override public String toString() {
        final StringBuilder builder = new StringBuilder();

        ListItem currentItem = this.first;
        while (currentItem != null) {
            builder.append(currentItem.getValue()).append(" ");
            currentItem = currentItem.getNext();
        }
        return builder.toString();
    }
}
