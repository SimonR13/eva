package da.lecture.rmi.callby.value;

import java.io.Serializable;

/**
 * Kettenglied einer einfach verketteten Liste, die von einem {@link ListItem}-Objekt verwaltet wird.
 */
public class ListItem implements Serializable {
    /** Wert. */
    private final int value;
    /** Nächstes Kettenglied der einfach verketteten Liste. */
    private ListItem next;

    /**
     * Initialisiert eine neue ListItem-Instanz mit den übergebenen Argumenten.
     * @param value Geünschter Wert.
     */
    public ListItem(final int value) {
        this.value = value;
    }

    /**
     * Liefert den Wert zurück.
     * @return Siehe Methodenbeschreibung
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Liefert der nächste Kettenglied der einfach verketteten Liste.
     * @return Siehe Methodenbeschreibung.
     */
    public ListItem getNext() {
        return this.next;
    }

    /**
     * Setzt das nächste Kettenglied der einfach verketteten Liste.
     * @param next Kettenglied, das als nächstes Kettenglied der einfach verketteten Liste gesetzt werden soll.
     */
    public void setNext(final ListItem next) {
        this.next = next;
    }
}
