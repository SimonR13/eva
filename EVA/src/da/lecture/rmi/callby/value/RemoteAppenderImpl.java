package da.lecture.rmi.callby.value;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Implementierung von {@link RemoteAppender} zu Manipulieren eines {@link List}-Objektes.
 */
public class RemoteAppenderImpl extends UnicastRemoteObject implements RemoteAppender {

    /**
     * Initialisiert eine neue RemoteAppenderImpl-Instanz.
     * @throws RemoteException Wird ausgelöst, wenn es beim Exportieren des RMI-Objektes, also beim Erzeugen des Skeletons, zu
     *             einem Fehler gekommen ist.
     */
    public RemoteAppenderImpl() throws RemoteException {
        // Muss wegen RemoteException von super() zur Verfügung gestellt werden.
    }

    @Override public void appendValue(final List listToAppendValueTo, final int valueToAppend) throws RemoteException {
        listToAppendValueTo.append(valueToAppend);
    }
}
