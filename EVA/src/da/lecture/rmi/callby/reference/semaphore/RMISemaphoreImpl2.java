package da.lecture.rmi.callby.reference.semaphore;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.Semaphore;

/**
 * Implementierung von {@link RMISemaphore}, die sämtliche Funktionalität für die Methoden aus {@link RMISemaphore} aus der Klasse
 * {@link Semaphore} erbt.
 */
public class RMISemaphoreImpl2 extends Semaphore implements RMISemaphore {
    /**
     * Initialisiert eine neue RMISemaphoreImpl2-Instanz mit den übergebenen Argumenten.
     * @param initialPermits Initial vorhandene Anzahl an Genehmigungen
     * @throws RemoteException Wird ausgelöst, wenn es beim Exportieren des RMI-Objektes, also beim Erzeugen des Skeletons, zu
     *             einem Fehler gekommen ist.
     */
    public RMISemaphoreImpl2(final int initialPermits) throws RemoteException {
        super(initialPermits);
        UnicastRemoteObject.exportObject(this, 0);
    }
}
