package da.lecture.rmi.callby.reference.semaphore;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * RMI-Schnittstelle für ein Semaphor, welches auf mehreren Rechnern für einen gegenseitigen Ausschluss auf einer gemeinsam
 * genutzten Ressource verwendet werden kann.
 */
public interface RMISemaphore extends Remote {
    /**
     * Bezieht eine Genhemigung von diesem Semaphor. Sollte aktuell keine Genehmigung mehr vorhanden sein, so verweilt der
     * aufrufende Thread in der Methode. Nach der Rückkehr aus dieser Methode und der damit erteilten Genehmigung darf der vom
     * Semaphor geschützte Bereich betreten werden.
     * @throws RemoteException Wird ausgelöst, wenn es bei der RMI-Kommunikation zu einem Fehler gekommen ist.
     * @throws InterruptedException Wird ausgelöst, wenn das Warten auf die Erteilung einer Genehmigung abgebrochen wurde,
     *             beispielsweise durch ein {@link Thread#interrupt()}.
     */
    void acquire() throws RemoteException, InterruptedException;

    /**
     * Gibt eine Genehmigung zurück und damit wieder frei für einen anderen Thread.
     * @throws RemoteException Wird ausgelöst, wenn es bei der RMI-Kommunikation zu einem Fehler gekommen ist.
     */
    void release() throws RemoteException;
}
