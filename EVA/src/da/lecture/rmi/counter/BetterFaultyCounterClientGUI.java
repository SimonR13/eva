package da.lecture.rmi.counter;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Grafische Benutzeroberfläche zur Anzeige und Manipulation eines {@link Counter}-Objektes, welches aus einer RMI-Regitry bezogen
 * wird. Der Fehler aus der Superklasse ist in dieser Implementierung behoben (Details zum Fehler finden sich in der
 * Klassenbeschreibung der Superklasse {@link FaultyCounterClientGUI}). Jedoch ist auch diese Version noch nicht ganz fehlerfrei.
 * Zwar werden die RMI-Methodenaufrufe nun in einem neuen Thread durchgeführt und blockieren so nicht mehr den Event Dispatch
 * Thread, allerdings werden nach dem RMI-Methodenaufruf auch Manipulationen an der grafischen Benutzeroberfläche direkt aus
 * diesem neuen Thread heraus durchgeführt. In der Konsequenz wird von mehreren Threads aus (Event Dispatch Thread und die von
 * dieser Klasse gestarteten Threads) parallel auf die Oberfläche zugegriffen und genau hierfür sind Swing-Oberflächen nicht
 * ausgelegt.
 */
public class BetterFaultyCounterClientGUI extends FaultyCounterClientGUI {
    /**
     * Initialisiert eine neue CounterClientGUI-Instanz mit den übergebenen Argumenten.
     * @param counter Zähler, dessen Wert angezeigt und manipuliert werden soll.
     */
    public BetterFaultyCounterClientGUI(final Counter counter) {
        super(counter);
    }

    @Override protected void onIncrementButtonClicked(final ActionEvent ev) {
        new Thread(this::doIncrementAndUpdateUi).start();
    }

    /**
     * Inkrementiert den akuellen Zählwert und zeigt anschließend den aktuellen Zählwert auf dem Label
     * {@link FaultyCounterClientGUI#counterLabel} an.
     */
    protected void doIncrementAndUpdateUi() {
        try {
            final int counterValueAfterIncrement = this.counter.increment();
            this.counterLabel.setText(String.valueOf(counterValueAfterIncrement));

        } catch (final RemoteException re) {
            JOptionPane.showMessageDialog(this, "Fehler: " + re.getMessage());
        }
    }

    @Override protected void onResetButtonClicked(final ActionEvent ev) {
        new Thread(this::doResetAndUpdateUi).start();
    }

    /**
     * Setzt den akuellen Zählwert zurück und zeigt anschließend den aktuellen Zählwert auf dem Label
     * {@link FaultyCounterClientGUI#counterLabel} an.
     */
    protected void doResetAndUpdateUi() {
        try {
            final int counterValueAfterIncrement = this.counter.reset();
            this.counterLabel.setText(String.valueOf(counterValueAfterIncrement));

        } catch (final RemoteException re) {
            JOptionPane.showMessageDialog(this, "Fehler: " + re.getMessage());
        }
    }

    //
    //

    /**
     * Hauptprogramm. Als Kommandozeilenargument muss die Adresse des Rechners angegeben werden, auf dem die RMI-Registry läuft.
     * @param args Kommandozeilenargumente.
     * @throws IOException Wird ausgelöst, wenn es bei der Kommunikation mit der RMI-Registry zu einem Ein-/Ausgabefehler gekommen
     *             ist.
     * @throws NotBoundException Wird ausgelöst, wenn bei der RMI-Registry auf dem Rechner mit der übergebenen Adresse kein Objekt
     *             mit dem Namen {@link Counter#DEFAULT_RMI_OBJECT_NAME} registriert ist.
     */
    public static void main(final String[] args) throws IOException, NotBoundException {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        final String objUrl = "rmi://" + args[0] + "/" + Counter.DEFAULT_RMI_OBJECT_NAME;
        final Counter counter = (Counter) Naming.lookup(objUrl);

        new BetterFaultyCounterClientGUI(counter);
    }
}
