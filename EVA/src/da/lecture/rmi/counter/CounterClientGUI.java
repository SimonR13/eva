package da.lecture.rmi.counter;

import java.awt.EventQueue;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Grafische Benutzeroberfläche zur Anzeige und Manipulation eines {@link Counter}-Objektes, welches aus einer RMI-Regitry bezogen
 * wird. Der Fehler aus der Superklasse ist in dieser Implementierung behoben (Details zum Fehler finden sich in der
 * Klassenbeschreibung der Superklasse {@link BetterFaultyCounterClientGUI}). Änderungen an der grafischen Benutzeroberfläche
 * werden nun nur noch vom Event Dispatch Thread ausgeführt, wodurch es nicht mehr zu parallelen Aufrufen auf der Oberfläche
 * kommen kann.
 */
public class CounterClientGUI extends BetterFaultyCounterClientGUI {
    /**
     * Initialisiert eine neue CounterClientGUI-Instanz mit den übergebenen Argumenten.
     * @param counter Zähler, dessen Wert angezeigt und manipuliert werden soll.
     */
    public CounterClientGUI(final Counter counter) {
        super(counter);
    }

    @Override protected void doIncrementAndUpdateUi() {
        try {
            final int counterValueAfterIncrement = this.counter.increment();
            EventQueue.invokeLater(() -> this.counterLabel.setText(String.valueOf(counterValueAfterIncrement)));

        } catch (final RemoteException re) {
            EventQueue.invokeLater(() -> JOptionPane.showMessageDialog(this, "Fehler: " + re.getMessage()));
        }
    }

    @Override protected void doResetAndUpdateUi() {
        try {
            final int counterValueAfterIncrement = this.counter.reset();
            EventQueue.invokeLater(() -> this.counterLabel.setText(String.valueOf(counterValueAfterIncrement)));

        } catch (final RemoteException re) {
            EventQueue.invokeLater(() -> JOptionPane.showMessageDialog(this, "Fehler: " + re.getMessage()));
        }
    }

    //
    //

    /**
     * Hauptprogramm. Als Kommandozeilenargument muss die Adresse des Rechners angegeben werden, auf dem die RMI-Registry läuft.
     * @param args Kommandozeilenargumente.
     * @throws IOException Wird ausgelöst, wenn es bei der Kommunikation mit der RMI-Registry zu einem Ein-/Ausgabefehler gekommen
     *             ist.
     * @throws NotBoundException Wird ausgelöst, wenn bei der RMI-Registry auf dem Rechner mit der übergebenen Adresse kein Objekt
     *             mit dem Namen {@link Counter#DEFAULT_RMI_OBJECT_NAME} registriert ist.
     */
    public static void main(final String[] args) throws IOException, NotBoundException {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        final String objUrl = "rmi://" + args[0] + "/" + Counter.DEFAULT_RMI_OBJECT_NAME;
        final Counter counter = (Counter) Naming.lookup(objUrl);

        new CounterClientGUI(counter);
    }
}
