package da.lecture.rmi.counter;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

/**
 * Fehlerhafte Grafische Benutzeroberfläche zur Anzeige und Manipulation eines {@link Counter}-Objektes, welches aus einer
 * RMI-Regitry bezogen wird. Die Adresse des Rechners, auf dem die RMI-Registry läuft muss als Kommandozeilenargument angegeben
 * werden.
 * <p>
 * Die Implementierung dieser grafischen Benutzeroberfläche ist insofern fehlerhaft, als dass der Event Dispatch Thread in den
 * beiden Ereignisbehandlungsmethoden {@link CounterClientFaultyGUI#onIncrementButtonClicked(ActionEvent))} und
 * {@link CounterClientFaultyGUI#onResetButtonClicked(ActionEvent))} direkt RMI-Methodenaufrufe durchführt und diese
 * beispielsweise aufgrund von Netzwerklatenzen nicht garantiert innerhalb eines Fingerschnipps abgearbeitet werden können. Sollte
 * es hier zu einer Verzögerung und damit zu einer längeren Blockade des Event Dispatch Thread kommen, würde die grafische
 * Benutzeroberfläche für den Zeitraum der Blockade einfrieren.
 */
public class FaultyCounterClientGUI extends JFrame {
    /** Zähler, dessen Wert angezeigt und manipuliert werden soll. */
    protected final Counter counter;
    /** Label zur Darstellung des Zählerwertes. */
    protected final JLabel counterLabel;

    /**
     * Initialisiert eine neue CounterClientFaultyGUI-Instanz mit den übergebenen Argumenten.
     * @param counter Zähler, dessen Wert angezeigt und manipuliert werden soll.
     */
    public FaultyCounterClientGUI(final Counter counter) {
        super("Grafischer Counter-Client");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        this.counter = counter;
        this.counterLabel = new JLabel("", SwingConstants.CENTER);

        final JButton incrementButton = new JButton("Erhöhen");
        incrementButton.addActionListener(this::onIncrementButtonClicked);

        final JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(this::onResetButtonClicked);

        this.setLayout(new GridLayout(0, 1));
        this.add(this.counterLabel);
        this.add(incrementButton);
        this.add(resetButton);
        this.setSize(300, 200);
        this.setVisible(true);
    }

    /**
     * Wird aufgerufen, sobald der Button zum Inkrementieren geklickt wurde und muss die Logik zum Inkrementieren und
     * anschließenden Anzeigen des neuen Zählwertes auf dem Label {@link FaultyCounterClientGUI#counterLabel} zur Verfügung
     * stellen.
     */
    protected void onIncrementButtonClicked(final ActionEvent ev) {
        try {
            final int counterValueAfterIncrement = this.counter.increment();
            this.counterLabel.setText(String.valueOf(counterValueAfterIncrement));

        } catch (final RemoteException re) {
            JOptionPane.showMessageDialog(this, "Fehler: " + re.getMessage());
        }
    }

    /**
     * Wird aufgerufen, sobald der Button zum Zurücksetzen geklickt wurde und muss die Logik zum Zurücksetzen und anschließenden
     * Anzeigen des neuen Zählwertes auf dem Label {@link FaultyCounterClientGUI#counterLabel} zur Verfügung stellen.
     */
    protected void onResetButtonClicked(final ActionEvent ev) {
        try {
            final int counterValueAfterReset = this.counter.reset();
            this.counterLabel.setText(String.valueOf(counterValueAfterReset));

        } catch (final RemoteException re) {
            JOptionPane.showMessageDialog(this, "Fehler: " + re.getMessage());
        }
    }

    //
    //

    /**
     * Hauptprogramm. Als Kommandozeilenargument muss die Adresse des Rechners angegeben werden, auf dem die RMI-Registry läuft.
     * @param args Kommandozeilenargumente.
     * @throws IOException Wird ausgelöst, wenn es bei der Kommunikation mit der RMI-Registry zu einem Ein-/Ausgabefehler gekommen
     *             ist.
     * @throws NotBoundException Wird ausgelöst, wenn bei der RMI-Registry auf dem Rechner mit der übergebenen Adresse kein Objekt
     *             mit dem Namen {@link Counter#DEFAULT_RMI_OBJECT_NAME} registriert ist.
     */
    public static void main(final String[] args) throws IOException, NotBoundException {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        final String objUrl = "rmi://" + args[0] + "/" + Counter.DEFAULT_RMI_OBJECT_NAME;
        final Counter counter = (Counter) Naming.lookup(objUrl);

        new FaultyCounterClientGUI(counter);
    }
}
