package da.lecture.rmi;

import java.awt.EventQueue;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JOptionPane;

public class RMIWorker {
    /** {@link ExecutorService} zur asynchronen Aufrühung der Aufrufe. */
    private static final ExecutorService EXECSERVICE = Executors.newCachedThreadPool();

    public interface RMIProducer<R> {
        R doRMICall() throws RemoteException;
    }

    public interface GUIConsumer<A> {
        void doGUIManipulation(final A argument);
    }

    public static <T> void doAsync(final RMIProducer<T> rmiProd, final GUIConsumer<T> guiCons) {
        new Thread(() -> RMIWorker.doDirect(rmiProd, guiCons)).start();
        RMIWorker.EXECSERVICE.execute(() -> RMIWorker.doDirect(rmiProd, guiCons));
    }

    public static <T> void doDirect(final RMIProducer<T> rmiProd, final GUIConsumer<T> guiCons) {
        try {
            final T rmiReturnValue = rmiProd.doRMICall();
            EventQueue.invokeLater(() -> guiCons.doGUIManipulation(rmiReturnValue));

        } catch (final RemoteException re) {
            EventQueue.invokeLater(() -> JOptionPane.showMessageDialog(null, "Fehler: " + re.getMessage()));
        }
    }
}
