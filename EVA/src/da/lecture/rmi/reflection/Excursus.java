package da.lecture.rmi.reflection;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.rmi.Remote;

/**
 * Kleine Beispielsklasse zur Demonstration des Prinzips hinter der Java Reflection API. Die beiden Schnittstellen wurden nur zur
 * Demonstration von {@link Class}-Objekten implementiert.
 */
public class Excursus implements Serializable, Remote {
    /**
     * Sinnfreier Konstruktor zur Demonstation eines {@link Constructor}-Objektes.
     * @param dummy Dummy-Zeichenkette.
     */
    public Excursus(final String dummy) {

    }

    /**
     * Sinnfreie Methode zur Demonstration der Verwendung eines {@link Method}-Objektes.
     * @param value Toller String-Parameter.
     * @return Gibt den Wert des tollen String-Parameters zurück.
     */
    public String dummyMethod(final String value) {
        return value;
    }

    /**
     * Hauptprogramm zur Veranschaulichung der Verwendung von {@link Class}, {@link Constructor} und {@link Method}-Objekten.
     * @param args Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws ReflectiveOperationException Wird ausgelöst, wenn es bei der Verwendung der Reflection zu einem Problem gekommen
     *             ist.
     */
    public static void main(final String[] args) throws ReflectiveOperationException {
        final Class<Excursus> excursusClass = Excursus.class;

        for (final Class<?> currentInterface : excursusClass.getInterfaces()) {
            System.out.println(currentInterface.toString());
        }

        final Constructor<Excursus> constr = excursusClass.getConstructor(String.class);
        final Excursus exc = constr.newInstance("Hallo");

        final Method dummyMethod = excursusClass.getMethod("dummyMethod", String.class);
        final String returnValue = (String) dummyMethod.invoke(exc, "Nochmal hallo");
        System.out.println(returnValue);
    }
}
