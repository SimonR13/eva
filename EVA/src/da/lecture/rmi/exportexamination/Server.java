package da.lecture.rmi.exportexamination;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Server-Klasse, die ein Objekt vom Typ {@link SomeRMIInterfaceImpl} bei einer lokal gestarteten RMI-Registry unter dem Namen
 * {@link SomeRMIInterface#DEFAULT_RMI_OBJECT_NAME} registriert.
 */
public class Server {
    /**
     * Hauptprogramm.
     * @param args Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws RemoteException Wird ausgelöst, wenn es bei der Kommunikation mit der RMI-Registry oder beim Exportieren der
     *             {@link SomeRMIInterfaceImpl}-Instanz zu einem Fehler gekommen ist.
     * @throws AlreadyBoundException Wird ausgelöst, wenn unter dem Namen {@link SomeRMIInterface#DEFAULT_RMI_OBJECT_NAME} bereits
     *             ein anderes RMI-Objekt bei der RMI-Registry registriert ist.
     */
    public static void main(final String[] args) throws RemoteException, AlreadyBoundException {
        final SomeRMIInterfaceImpl someObj = new SomeRMIInterfaceImpl();
        UnicastRemoteObject.exportObject(someObj, 0);

        final Registry localReg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        localReg.bind(SomeRMIInterface.DEFAULT_RMI_OBJECT_NAME, someObj);
    }
}
