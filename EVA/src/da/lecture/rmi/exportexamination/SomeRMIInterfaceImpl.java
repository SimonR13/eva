package da.lecture.rmi.exportexamination;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Implementierung von {@link SomeRMIInterface}, die extra nicht aus {@link UnicastRemoteObject} abgeleitet ist, damit der Export
 * eines Objektes dieser Klasse nicht automatisch bei Instanziierung geschieht, sondern selbst zu einem gewünschten Zeitpunkt über
 * {@link UnicastRemoteObject#exportObject(Remote, int)} durchgeführt werden kann. Um zu demonstrieren, dass ein nicht
 * exportiertes Objekt dieser Klasse per Wert übertragen wird, ist ausßerdem die Schnittstelle {@link Serializable} implementiert.
 */
public class SomeRMIInterfaceImpl implements SomeRMIInterface, Serializable {
    @Override public void someMethod(final SomeRMIInterface obj) throws RemoteException {
        System.out.println(obj.getClass().getName());
    }
}
