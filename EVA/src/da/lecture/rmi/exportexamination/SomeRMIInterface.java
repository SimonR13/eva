package da.lecture.rmi.exportexamination;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Einfache RMI-Schnittelle mit sinnfreier Methode zur Demonstration der Wirklung von
 * {@link UnicastRemoteObject#exportObject(Remote, int)}.
 */
public interface SomeRMIInterface extends Remote {
    /** Konstante für den Standardnamen eines SomeRMIInterface-Objektes in einer RMI-Registry. */
    String DEFAULT_RMI_OBJECT_NAME = "SomeName";

    /**
     * Vollkommen sinnfreie Methode. Hauptsache, es kann ein Objekt als Parameter übergeben werden, damit die Wirkung von
     * {@link UnicastRemoteObject#exportObject(Remote, int)} bei der Parameterübergabe demonstriert werden kann.
     * @param obj Tolles Objekt.
     * @throws RemoteException Wird ausgelöst, wenn es bei der RMI-Kommunikation zu einem Fehler gekommen ist.
     */
    void someMethod(SomeRMIInterface obj) throws RemoteException;
}
