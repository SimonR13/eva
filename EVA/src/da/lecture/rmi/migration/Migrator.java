package da.lecture.rmi.migration;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * RMI-Schnittstelle für den Migrator.
 */
public interface Migrator extends Remote {
    /** Konstante für den Standardnamen eines RemoteAppender-Objektes in einer RMI-Registry. */
    String DEFAULT_RMI_OBJECT_NAME = "Migrator";

    /**
     * Nimmt ein Objekt vom Typ {@link Remote} entgegen, exportiert es und liefert einen Stub auf das exportierte Objekt zurück.
     * @param objectToMigrate {@link Remote}-Objekt, das migriert werden soll.
     * @return Stub-Objekt für das migrierte {@link Remote}-Objekt.
     * @throws RemoteException Wird ausgelöst, wenn es bei der RMI-Kommunikation zu einem Fehler gekommen ist.
     */
    Remote migrate(Remote objectToMigrate) throws RemoteException;
}
