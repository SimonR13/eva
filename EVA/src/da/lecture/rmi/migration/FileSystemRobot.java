package da.lecture.rmi.migration;

import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 * RMI-Schnittstelle für einen {@link FileSystemRobot}, welcher über einen {@link Migrator} auf verschiedene Rechner migriert
 * werden und dort zur ferngesteuerten Durchführung von Datensystemoperationen verwendet werden kann.
 */
public interface FileSystemRobot extends Remote {
    /**
     * Sucht nach Dateien mit der übergebenen Endung und liefert diese zurück.
     * @param ending Dateiendung.
     * @return Liste mit gefundenen Dateien, die die übergebene Endung aufweisen.
     * @throws RemoteException Wird ausgelöst, wenn es bei der RMI-Kommunikation zu einem Fehler gekommen ist.
     */
    List<File> findFilesEndingWith(final String ending) throws RemoteException;

    /**
     * Löscht die übergebene Datei.
     * @param fileToDelete Zu löschende Datei.
     * @return <code>true</code>, wenn das Löschen erfolgreich war, <code>false</code> sonst.
     * @throws RemoteException Wird ausgelöst, wenn es bei der RMI-Kommunikation zu einem Fehler gekommen ist.
     */
    boolean deleteFile(final File fileToDelete) throws RemoteException;

    /**
     * Schreibt ein Protokoll von ausgeführten Operationen auf {@link System#out}.
     * @throws RemoteException Wird ausgelöst, wenn es bei der RMI-Kommunikation zu einem Fehler gekommen ist.
     */
    void printLog() throws RemoteException;

    /**
     * Führt ein {@link UnicastRemoteObject#unexportObject(Remote, boolean)} auf diesem Objekt aus und liefert das Objekt danach
     * zurück. Da das Objekt beim Zurückliefern kein exportiertes Objekt mehr ist, erfolgt die Rücklieferung per Wert. Es wird
     * also eine Kopie des gesamten Objektes zurückgeliefert.
     * @return Siehe Methodenbeschreibung.
     * @throws RemoteException Wird ausgelöst, wenn es bei der RMI-Kommunikation zu einem Fehler gekommen ist.
     */
    FileSystemRobot comeBack() throws RemoteException;
}
