package da.lecture.rmi.migration;

import java.io.File;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.NoSuchObjectException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementierung von {@link FileSystemRobot}. Diese Implementierung ist nicht aus {@link UnicastRemoteObject} abgeleitet. Es
 * muss als nach der Erzeugung selbst für den export des Objektes zum gewünschten Zeitpunkt gesorgt werden.
 */
public class FileSystemRobotImpl implements FileSystemRobot, Serializable {
    /** Speichert ein Verwendungsprotokoll ab. */
    private final StringBuilder log;

    /**
     * Initialisiert eine neue FileSystemRobotImpl-Instanz.
     */
    public FileSystemRobotImpl() {
        this.log = new StringBuilder();
    }

    @Override public List<File> findFilesEndingWith(final String ending) {
        this.log("findFilesEndingWith mit " + ending + " aufgerufen");
        System.out.println("Und los gehts!");

        final List<File> foundFiles = new LinkedList<>();
        for (final File currentRoot : File.listRoots()) {
            this.findFilesEndingWith(ending, currentRoot, foundFiles);
        }
        return foundFiles;
    }

    /**
     * Rekrusive Hilfsmethode, die in dem übergebenen Verzeichnis nach Dateien mit der übergebenen Dateiendung sucht und diese
     * dann in der übergebenen Liste abalegt.
     * @param ending Dateiendung, nach der Ausschau gehalten werden soll.
     * @param directory Verzeichnis, welches rekusriv durchsucht werden soll.
     * @param findingsList Liste, in die Dateien mit der übergebenen Dateiendung eingetragen werden sollen.
     */
    private void findFilesEndingWith(final String ending, final File directory, final List<File> findingsList) {
        final File[] directoryContents = directory.listFiles();
        if (directoryContents != null) {
            for (final File currentItem : directoryContents) {
                if (currentItem.isDirectory()) {
                    this.findFilesEndingWith(ending, currentItem, findingsList);

                } else {
                    if (currentItem.getName().endsWith(ending)) {
                        findingsList.add(currentItem);
                    }
                }
            }
        }
    }

    @Override public boolean deleteFile(final File fileToDelete) {
        this.log("fileToDelete aufgerufen");
        return fileToDelete.delete();
    }

    @Override public FileSystemRobot comeBack() throws NoSuchObjectException {
        this.log("comeBack aufgerufen");
        UnicastRemoteObject.unexportObject(this, true);

        // Wegen vorherigem unexport wird jetzt eine Kopie dieses Objektes zurückgeliefert und kein Stub
        return this;
    }

    @Override public synchronized void printLog() {
        System.out.println(this.log.toString());
    }

    /**
     * Hilfsmethode, welche dem Protokoll in {@link #log} einen neuen Eintrag hinzufügt.
     * @param message Hinzuzufügender Eintrag.
     */
    private synchronized void log(final String message) {
        String hostName = "UnknownHost";
        try {
            final InetAddress localHost = InetAddress.getLocalHost();
            hostName = localHost.getHostName() + " (" + localHost.getHostAddress() + ")";

        } catch (final UnknownHostException uhe) {
            // Wird ausgelöst, wenn es Probleme bei der Namensauflösung des Rechners gab. In einem solchen Fall bleibt der Wert
            // von localHostName auf dem Standard.
        }

        this.log.append(DateFormat.getInstance().format(new Date())).append(": ");
        this.log.append(message).append(", Objektstandort ").append(hostName).append("\n");
    }
}
