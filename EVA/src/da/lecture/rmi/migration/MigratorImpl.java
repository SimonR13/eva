package da.lecture.rmi.migration;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Implementierung von {@link Migrator} zur Migration von RMI-Objekten.
 */
public class MigratorImpl extends UnicastRemoteObject implements Migrator {
    /**
     * Initialisiert eine neue MigratorImpl-Instanz.
     * @throws RemoteException Wird ausgelöst, wenn es beim Exportieren dieses RMI-Objektes zu einem Fehler gekommen ist.
     */
    public MigratorImpl() throws RemoteException {
        super();
    }

    @Override public Remote migrate(final Remote robotToMigrate) {
        try {
            UnicastRemoteObject.exportObject(robotToMigrate, 0);
        } catch (final RemoteException re) {
            // Fehler beim Exportieren des Objektes. Dies wird mit einer anderen Ausnahme als einer RemoteException an den
            // Aufrufer gemeldet, weil der sonst davon ausgehen könnte, dass die Fehlerursache ein Fehler in der Kommunikation
            // zwischen ihm und dem Migrator sein könnte.
            throw new RuntimeException("Objekt konnte vom Migrator nicht exportiert werden!", re);
        }

        // Wegen vorheriger Exportierung wird jetzt ein Stub zurückgeliefert
        return robotToMigrate;
    }
}
