package da.lecture.rmi.migration;

import java.io.File;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

/**
 * Client-Klasse, die ein {@link FileSystemRobotImpl} erzeugt, auf verschiedene Rechner migriert und dort nach txt-Dateien suchen
 * lässt. Die Adressen der Rechner müssen über die Komandozeile angegeben werden.
 */
public class Client {
    /**
     * Hauptprogramm. Als Kommandozeilenargumente müssen die Namen bzw. Adressen der Rechner angegeben werden, die nach
     * txt-Dateien durchsucht werden sollen. Es wird erwartet, dass auf diesen Rechnern eine RMI-Registry auf dem Standardport
     * läuft und einen {@link Migrator} gebunden an den Namen {@link Migrator#DEFAULT_RMI_OBJECT_NAME} enthält.
     * @param args Kommandozeilenargumente.
     * @throws RemoteException Wird ausgelöst, wenn es zu einem RMI spezifischen Fehler gekommen ist.
     * @throws NotBoundException Wird ausgelöst, wenn in einer Registry kein {@link Migrator} an den Namen
     *             {@link Migrator#DEFAULT_RMI_OBJECT_NAME} gebunden ist.
     */
    public static void main(final String[] args) throws RemoteException, NotBoundException {
        FileSystemRobot robot = new FileSystemRobotImpl();

        for (final String currentHostAddress : args) {
            final Registry hostReg = LocateRegistry.getRegistry(currentHostAddress);
            final Migrator hostMigrator = (Migrator) hostReg.lookup(Migrator.DEFAULT_RMI_OBJECT_NAME);

            robot = (FileSystemRobot) hostMigrator.migrate(robot);
            final List<File> foundFiles = robot.findFilesEndingWith(".txt");

            System.out.println("Gefunden auf " + currentHostAddress);
            for (final File file : foundFiles) {
                System.out.println(file);
            }
            System.out.println();

            robot = robot.comeBack();
        }

        System.out.println("Ausführungsprotokoll:");
        robot.printLog();
    }
}
