package da.lecture.rmi.compression;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketImpl;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Implementierung von {@link RMIClientSocketFactory} und {@link RMIServerSocketFactory} zur Komprimierung der RMI-Kommunikation.
 */
public class CompressingSocketFactory implements RMIClientSocketFactory, RMIServerSocketFactory, Serializable {

    @Override public Socket createSocket(final String host, final int port) throws IOException {
        return new CompressingSocket(host, port);
    }

    @Override public ServerSocket createServerSocket(final int port) throws IOException {
        return new CompressingServerSocket(port);
    }

    //
    //

    /**
     * Ableitung von {@link Socket}, die komprimierende und dekomprimierende Ströme zu Kommunikation zurückliefert.
     */
    private static class CompressingSocket extends Socket {
        /**
         * Initialisiert eine neue CompressingSocket-Instanz mit den übergebenen Argumenten.
         * @param host Adresse des Rechners, mit dem kommunziert werden soll.
         * @param port Portnummer, mit der das Socket auf dem übergebenen Rechner verbunden werden soll.
         * @throws IOException Wird ausgelöst, wenn es beim Verbindungsaufbau zu einem Problem gekommen ist.
         */
        public CompressingSocket(final String host, final int port) throws IOException {
            super(host, port);
        }

        /**
         * Initialisiert eine neue CompressingSocket-Instanz mit den übergebenen Argumenten.
         * @param impl {@link SocketImpl}-Instan, die für die Kommunikation
         * @throws IOException Wird ausgelöst, wenn es einen Fehler im TCP-Protokoll-Stack gegeben hat.
         */
        public CompressingSocket(final SocketImpl impl) throws IOException {
            super(impl);
        }

        @Override public OutputStream getOutputStream() throws IOException {
            return new GZIPOutputStream(super.getOutputStream(), true);
        }

        @Override public InputStream getInputStream() throws IOException {
            return new GZIPInputStream(super.getInputStream());
        }
    }

    /**
     * Ableitung von {@link ServerSocket}, die bei einem {@link CompressingServerSocket#accept()} ein
     * {@link CompressingServerSocket} zurückliefert.
     */
    private static class CompressingServerSocket extends ServerSocket {
        /**
         * Initialisiert eine neue CompressingServerSocket-Instanz mit den übergebenen Argumenten.
         * @param port Port, auf dem Verbindungen angenommen werden sollen.
         * @throws IOException Wird ausgelöst, wenn es beim Öffnen des Sockets zu einem Problem gekommen ist.
         */
        public CompressingServerSocket(final int port) throws IOException {
            super(port);
        }

        @Override public Socket accept() throws IOException {
            if (this.isClosed()) {
                throw new SocketException("Socket is closed");
            }
            if (!this.isBound()) {
                throw new SocketException("Socket is not bound yet");
            }
            final CompressingSocket s = new CompressingSocket((SocketImpl) null);
            this.implAccept(s);
            return s;
        }
    }
}
