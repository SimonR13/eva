package da.lecture.rmi.lamda;

public class ExcursusRunnable {
    private static void runConcurrently(final Runnable runnable) {
        new Thread(runnable).start();
    }

    public static void main(final String[] args) {
        ExcursusRunnable.runConcurrently(new Runnable() {
            @Override public void run() {
                System.out.println("Nebenläufige Ausgabe");
            }
        });

        ExcursusRunnable.runConcurrently(() -> System.out.println("Nebenläufige Ausgabe"));
        ExcursusRunnable.runConcurrently(ExcursusRunnable::toBeExecutedConcurrently);
    }

    private static void toBeExecutedConcurrently() {
        System.out.println("Nebenläufige Ausgabe");
    }
}
