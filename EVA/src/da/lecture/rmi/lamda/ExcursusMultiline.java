package da.lecture.rmi.lamda;

public class ExcursusMultiline {
    private static int x = 5;

    public static void main(final String[] args) {
        final int y = 10;
        new Thread(() -> {
            final int result = ExcursusMultiline.x * y;
            System.out.println(result);
        }).start();
    }
}
