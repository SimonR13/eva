package da.lecture.rmi.anonymousclasses;

/**
 * Kleine Beispielsklasse zur Demonstration des Prinzips hinter annonymen Klassen.
 */
public class Excursus {
    /** Attribut zur Demonstration des Zugriffs aus einer annonymen Klasse heraus. */
    private final String outerAttribute;

    /**
     * Initialisiert eine neue Excursus-Instanz.
     */
    public Excursus() {
        this.outerAttribute = "Attribut aus umschließendem Objekt";
        final String localVariable = "Lokale Variable des Konstruktors";

        final Runnable runa = new Runnable() {
            @Override public void run() {
                System.out.println(localVariable);
                System.out.println(Excursus.this.outerAttribute);

                Excursus.this.outerMethod(localVariable);
            }
        };
        runa.run();
    }

    /**
     * Methode zur Veranschaulichung, dass man aus annonymen Klassen heraus private Methoden der umschließenen Klasse aufrufen
     * kann.
     * @param param Demonstrationsparameter
     */
    private void outerMethod(final String param) {

    }

    /**
     * Hauptprogramm.
     * @param args Kommandozeilenparameter (hier nicht benötigt).
     */
    public static void main(final String[] args) {
        Object obj = new Object();
        System.out.println(obj.toString());

        obj = new Object() {
            @Override public String toString() {
                return "Ich bin ein Objekt einer anonymen Klasse!";
            }
        };
        System.out.println(obj.toString());

        final Runnable runa = new Runnable() {
            @Override public void run() {
                System.out.println("Anonyme Klasse, die Schnittstelle Runnabe implementiert");
            }
        };
        runa.run();
    }
}
