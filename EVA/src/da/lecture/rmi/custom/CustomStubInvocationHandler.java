package da.lecture.rmi.custom;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.rmi.server.ObjID;

/**
 * {@link InvocationHandler}-Implementierung für dynamische Proxies, die als Stub für entfernte Objekte in einem
 * {@link CustomSkeleton} dienen.
 */
public class CustomStubInvocationHandler implements InvocationHandler, Serializable {
    /** Kennung des entfernten Objektes. */
    private final ObjID remoteObjId;
    /** Adresse und Port für die Kontaktaufnahme mit dem Skeleton. */
    private final InetSocketAddress skelAddr;

    /**
     * Initialisiert eine neue CustomStubInvocationHandler-Instanz mit den übergebenen Argumenten.
     * @param objId Kennung des entfernten Objektes.
     * @param skeletonAddress Adresse und Port für die Kontaktaufnahme mit dem Skeleton.
     */
    public CustomStubInvocationHandler(final ObjID objId, final InetSocketAddress skeletonAddress) {
        this.remoteObjId = objId;
        this.skelAddr = skeletonAddress;
    }

    @Override public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        try (final Socket s = new Socket(this.skelAddr.getAddress(), this.skelAddr.getPort())) {
            final ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
            out.writeObject(this.remoteObjId); // Objektkennung schicken
            out.writeUTF(method.getName()); // Name der aufzurufenden Methode schicken
            out.writeObject(method.getParameterTypes()); // Parameter-Typen der Methode schicken
            out.writeObject(args); // Aufruf-Argumente schicken

            final ObjectInputStream in = new ObjectInputStream(s.getInputStream());
            final Object returnValue = in.readObject(); // Auf Rückgabewert warten
            return returnValue; // Rückgabewert an Proxy und damit an Aufrufer zurückgeben
        }
    }
}
