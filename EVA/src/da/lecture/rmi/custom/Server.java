package da.lecture.rmi.custom;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import da.lecture.rmi.exportexamination.SomeRMIInterface;
import da.lecture.rmi.exportexamination.SomeRMIInterfaceImpl;

/**
 * Server zur Veranschaulichung der Verwendung der eigenen Stub- und Skeleton-Implementierung aus {@link CustomSkeleton} und
 * {@link CustomStubInvocationHandler}.
 */
public class Server {
    /**
     * Hauptprogramm.
     * @param args Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws RemoteException Wird ausgelöst, wenn es bei der Kommunikation mit der RMI-Registry oder beim Exportieren der
     *             {@link SomeRMIInterfaceImpl}-Instanz zu einem Fehler gekommen ist.
     * @throws AlreadyBoundException Wird ausgelöst, wenn unter dem Namen {@link SomeRMIInterface#DEFAULT_RMI_OBJECT_NAME} bereits
     *             ein anderes RMI-Objekt bei der RMI-Registry registriert ist.
     */
    public static void main(final String[] args) throws RemoteException, AlreadyBoundException {
        final SomeRMIInterfaceImpl someObj = new SomeRMIInterfaceImpl();
        final SomeRMIInterface stub = (SomeRMIInterface) CustomSkeleton.export(someObj);

        final Registry localReg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        localReg.bind(SomeRMIInterface.DEFAULT_RMI_OBJECT_NAME, stub);
    }
}
