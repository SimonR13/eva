package da.lecture.rmi.dynamicproxy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Beispielklasse zur Veranschaulichung der Verwendung von dynamischen Proxies.
 */
public class Excursus {
    /**
     * Hauptprogramm.
     * @param args Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws ReflectiveOperationException Wird ausgelöst, wenn es bei der Verwendung der Reflection zu einem Problem gekommen
     *             ist.
     */
    public static void main(final String[] args) throws ReflectiveOperationException {
        final Class<?> proxyClass = Proxy.getProxyClass(Excursus.class.getClassLoader(), SampleInterface.class);
        final InvocationHandler invHandler = new InvocationHandler() {
            @Override public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
                System.out.println("Aufruf " + method.getName());

                if (method.getName().equals("someOtherMethod")) {
                    return "Sauber!";
                }
                return null;
            }
        };

        final Constructor<?> proxyConstr = proxyClass.getConstructor(InvocationHandler.class);
        final SampleInterface proxy = (SampleInterface) proxyConstr.newInstance(invHandler);
        proxy.someMethod(); // Ausgabe "Aufruf someMethod"
        final String retVal = proxy.someOtherMethod("Hi"); // Ausgabe "Aufruf someOtherMethod"
        System.out.println(retVal); // Ausgabe "Sauber!"
    }
}
