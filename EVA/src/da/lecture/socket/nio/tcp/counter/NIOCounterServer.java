package da.lecture.socket.nio.tcp.counter;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * TCP-Server, der einen int-Zähler verwaltet und die Befehle "increment" und
 * "reset" versteht. Auf jeden Befehl wird der Zählerstand nach dem Befehl
 * geantwortet. Zur Umsetzung der TCP_Kommunikation mit den Clients wurden ein
 * {@link ServerSocketChannel} und ein {@link Selector} verwendet, so dass der
 * Server mit nur einem einzelnen Threads mehrere Clients gleichzeitig bedienen
 * kann.
 */
public class NIOCounterServer
{
    /** Port, auf dem der TCP-Server auf Befehle lauscht. */
    public static final int PORT = 1250;

    /** Zeichensatz für die Kodierung der Befehle. */
    public static final Charset CHARSET = Charset.forName("UTF-8");

    /** Verwalteter int-Zähler. */
    private static int counter;

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation mit einem
     *             Client zu einem Ein-/Ausgabefehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final ServerSocketChannel sSocket = ServerSocketChannel.open();
        final Selector selector = Selector.open())
        {
            sSocket.bind(new InetSocketAddress(NIOCounterServer.PORT));
            sSocket.configureBlocking(false);
            sSocket.register(selector, SelectionKey.OP_ACCEPT);

            while (true)
            {
                NIOCounterServer.processSelectionKeys(selector);
            }
        }
    }

    /**
     * Hilfsmethode, die in einer Schleife aufgerufen werden kann, Ereignisse
     * vom übergebenen Selector abholt und diese Abarbeitet. Neu angenommene
     * Verbindungen werden ebenfalls mit diesem Selektor verknüpft.
     * 
     * @param selector
     *            {@link Selector}, von dem Ereignisse abgeholt und bearbeitet
     *            werden sollen.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation mit einem
     *             Client zu einem Ein-/Ausgabefehler gekommen ist.
     */
    private static void processSelectionKeys(final Selector selector) throws IOException
    {
        selector.select();
        final Iterator<SelectionKey> keyIterator = selector.selectedKeys().iterator();
        while (keyIterator.hasNext())
        {
            final SelectionKey currentKey = keyIterator.next();
            keyIterator.remove();

            if (currentKey.isAcceptable())
            {
                NIOCounterServer.doAccept(currentKey);
            }
            else if (currentKey.isReadable())
            {
                NIOCounterServer.doRead(currentKey);
            }
            else if (currentKey.isWritable())
            {
                NIOCounterServer.doWrite(currentKey);
            }
        }
    }

    /**
     * Hilfsmethode, die immer dann aufgerufen wird, wenn eine
     * Verbindungsanfrage von einem Client angenommen werden soll.
     * 
     * @param key
     *            Ereignis, welches die Verbindungsanfrage symbolisiert und
     *            Informationen zu dieser bereitstellt.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation mit einem
     *             Client zu einem Ein-/Ausgabefehler gekommen ist.
     */
    private static void doAccept(final SelectionKey key) throws IOException
    {
        // Gemeldete Verbindung annehmen, als nicht blockierend Konfigurieren
        // und dem Selektor zuweisen.
        final SocketChannel socketToClient = ((ServerSocketChannel) key.channel()).accept();
        socketToClient.configureBlocking(false);
        final SelectionKey clientKey = socketToClient.register(key.selector(), SelectionKey.OP_READ);

        // Objekt zur Kapselung der Datenpuffer an den Selektionsschlüssel der
        // angenommenen Verbindung anhängen.
        clientKey.attach(new ConnectionData());
    }

    /**
     * Hilfsmethode, die immer dann aufgerufen wird, wenn Daten von einem Client
     * eingetroffen sind und zum Lesen zur Verfügung stehen.
     * 
     * @param key
     *            Ereignis, welches das Ankommen neuer Daten symbolisiert und
     *            Informationen hierzu bereitstellt.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation mit einem
     *             Client zu einem Ein-/Ausgabefehler gekommen ist.
     */
    private static void doRead(final SelectionKey key) throws IOException
    {
        final ByteBuffer inBuff = ByteBuffer.allocate(1024);
        final int readBytes = ((SocketChannel) key.channel()).read(inBuff);

        // Konnten keine Bytes mehr gelesen werden bedeutet dies, dass der
        // Client die Verbindung abgebaut hat. Also schließen wir
        // die Verbidnung auf Serverseite ebenfalls und melden sie durch ein
        // key.cancel() vom Selector ab.
        if (readBytes == -1)
        {
            key.channel().close();
            key.cancel();
            return;
        }

        inBuff.flip();
        final CharBuffer readChars = NIOCounterServer.CHARSET.decode(inBuff);
        final ConnectionData connData = (ConnectionData) key.attachment();
        while (readChars.hasRemaining())
        {
            final char currentChar = readChars.get();
            if (currentChar != '\n')
            {
                // Solange kein Trennzeichen gelesen wird, werden die Zeichen an
                // den Eingangspuffer angefügt.
                connData.inBuffer.append(currentChar);

            }
            else
            {
                // Sobald ein Trennzeichen gelesen wurde steht im Eingangspuffer
                // ein vollständiger Befehl. Dieser wird extrahiet,
                // abgearbeitet und die Antwort wird in einen ByteBuffer
                // konvertiert.
                final String request = connData.inBuffer.toString().trim();
                final String answer = NIOCounterServer.processRequestAndReturnAnswer(request) + "\n";
                final ByteBuffer sendBuffer = NIOCounterServer.CHARSET.encode(CharBuffer.wrap(answer));

                // Antwort wird im Ausgangspuffer hinterlegt und der
                // Eingangspuffer wird geleert.
                connData.outBuffer.addLast(sendBuffer);
                connData.inBuffer.setLength(0);
            }
        }

        // Sollten sich nach dem Lesen Daten im Ausgangspuffer befinden, so
        // müssen diese in absehbarer Zeit an den Client
        // versendet werden. Hierzu teilen wir dem Selektor mit, dass wir
        // darüber informiet werden möchten, sobald die Verbindung
        // zum Client für das Schreiben bereit ist.
        if (!connData.outBuffer.isEmpty())
        {
            key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
        }
    }

    /**
     * Hilfsmethode, die immer dann aufgerufen wird, wenn Daten an einen Client
     * gesendet werden können.
     * 
     * @param key
     *            Ereignis, welches die Sendemöglichkeit von Daten symbolisiert
     *            und Informationen hierzu bereitstellt.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation mit einem
     *             Client zu einem Ein-/Ausgabefehler gekommen ist.
     */
    private static void doWrite(final SelectionKey key) throws IOException
    {
        final SocketChannel socketToClient = (SocketChannel) key.channel();
        final ConnectionData connData = (ConnectionData) key.attachment();

        // Alle aktuell vorhendenen Schreibpuffer durchlaufen und Inhalte dieser
        // Puffer in das Socket schreiben.
        while (!connData.outBuffer.isEmpty())
        {
            final ByteBuffer currentBuffer = connData.outBuffer.removeFirst();
            socketToClient.write(currentBuffer);

            // Sollten beim Schreiben der Pufferinhalte in das Socket nicht der
            // gesamte Inhalt geschrieben worden sein, so kann
            // dies beim Puffer erfragt werden. In einem solchen Fall wird der
            // Puffer am Kopf der Warteschlange aller
            // abzuarbeitenden Schreibpuffer eingefügt, so dass er bei der
            // nächsten Benachrichtigung der Schreibbereitschaft
            // erneut betrachtet wird. Die Position, bis zu der die
            // Pufferinhalte bereis in das Socket geschrieben wurden,
            // verwaltet sich der Puffer selbst.
            if (currentBuffer.hasRemaining())
            {
                connData.outBuffer.addFirst(currentBuffer);
                break;
            }
        }

        // Wenn der Schreibpuffer leer ist, dann gibt es aktuell keine Daten
        // mehr, die an den Client gesendet werden sollen.
        // In diesem Fall wird dem Selektor gemeldet, dass wir nicht mehr an
        // Meldungen zur Schreibbereitschaft des Sockets
        // interessiert sind. Würde dieser Schritt hier nicht erfolgen, würden
        // wir im Nachfolgenden nach Leerung des Sockets
        // andauernd über die Schreibbereitschaft informiert, obwohl es nichts
        // zu schreiben gibt.
        if (connData.outBuffer.isEmpty())
        {
            key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE);
        }
    }

    /**
     * Hilfsmethode, die einen übergebenen Befehl ausführt und den neuen
     * Zählerstand als Antwort zurückliefert.
     * 
     * @param request
     *            Auszuführender Befehl.
     * @return Zählerstand nach Ausführung des Befehls.
     */
    private static String processRequestAndReturnAnswer(final String request)
    {
        if (request.equals("increment"))
        {
            NIOCounterServer.counter++;
        }
        else if (request.equals("reset"))
        {
            NIOCounterServer.counter = 0;
        }

        return String.valueOf(NIOCounterServer.counter);
    }

    //
    //

    /**
     * Hilfsklasse, welche Datenpuffer für eingehende und ausgehende Daten einer
     * Verbindung zur Verfügung stellt.
     */
    private static class ConnectionData
    {
        // CHECKSTYLE:OFF
        /** Eingangspuffer. */
        public final StringBuilder inBuffer = new StringBuilder(20);

        /** Ausgangspuffer. */
        public final Deque<ByteBuffer> outBuffer = new LinkedList<>();
        // CHECKSTLE:ON
    }
}
