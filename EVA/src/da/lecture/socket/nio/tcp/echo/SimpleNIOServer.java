package da.lecture.socket.nio.tcp.echo;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

/**
 * Einfacher NIO-TCP-Server zur Veranschaulichung des prinzipiellen Aufbaus
 * einer NIO-TCP-Serveranwendung. Der Server horcht auf Port 1250 auf ankommende
 * Verbindungsanfragen. Die über angenommene Verbindungen ausgetauschenten Daten
 * werden als UTF-8-kodierte Zeichenektten auffasst. Vor eine solche
 * Zeichenektte wird die Zeichenkette "Selber " gehängt und das Ergebnis wird
 * UTF-8-kodiert an den Client zurückgesendet.
 */
public class SimpleNIOServer
{
    /**
     * Hilfsmethode zur Abarbeitung einer Anfrage, die als Zeichenkette vom
     * übergebenen {@link SocketChannel} gelesen wird.
     * 
     * @param socket
     *            {@link SocketChannel}, von dem die Anfrage eines Clients
     *            gelesen und über das die Antwort an den anfragenden Client
     *            gesendet werden soll.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link SocketChannel} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    private static void processConnection(final SocketChannel socket) throws IOException
    {
        final Charset charset = Charset.forName("UTF-8");
        final ByteBuffer inBuffer = ByteBuffer.allocate(1024);
        socket.read(inBuffer); // Blockiert bis Daten empfangen wurden
        inBuffer.flip();
        final String request = charset.decode(inBuffer).toString();

        final String answer = "Selber " + request;
        final ByteBuffer outBuffer = charset.encode(answer);
        socket.write(outBuffer); // Blockiert bis Daten geschrieben wurden
    }

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über den
     *             verwendeten {@link ServerSocketChannel} beziehungsweise
     *             {@link SocketChannel} zu einem Ein-/Ausgabefehler gekommen
     *             ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final ServerSocketChannel sSocket = ServerSocketChannel.open())
        {
            sSocket.bind(new InetSocketAddress(1250));

            while (true)
            {
                try (final SocketChannel socketToClient = sSocket.accept())
                {
                    SimpleNIOServer.processConnection(socketToClient);
                } // Verbindung mit Client wird abgebaut und Datenkanäle werden
                  // automatisch geschlossen
            }
        }
    }
}
