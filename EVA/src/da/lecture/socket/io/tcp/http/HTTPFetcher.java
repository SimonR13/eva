package da.lecture.socket.io.tcp.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Klasse, deren main-Methode eine GET-Anfrage nach der Resource index.php an
 * den Web-Server www.hochschlue-trier.de sendet und die Antwort auf der Konsole
 * ausgibt. Dabei wird die Klasse {@link HttpURLConnection} verwendet.
 */
public class HTTPFetcher
{
    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation mit dem
     *             Webserver zu einem Ein-/Ausgabefehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final URL url = new URL("http", "www.hochschule-trier.de", "/index.php");

        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), Charset.forName("UTF-8"))))
        {
            String readLine = null;
            while ((readLine = reader.readLine()) != null)
            {
                System.out.println(readLine);
            }
        }
    }
}
