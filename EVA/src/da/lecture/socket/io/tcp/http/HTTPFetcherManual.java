package da.lecture.socket.io.tcp.http;

import java.io.IOException;

import da.lecture.socket.io.tcp.TCPSocket;

/**
 * Klasse, deren main-Methode eine get-Anfrage nach der Resource index.php an
 * den Web-Server www.hochschule-trier.de sendet und die Antwort auf der Konsole
 * ausgibt.
 */
public class HTTPFetcherManual
{
    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link TCPSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final TCPSocket tcpSocket = new TCPSocket("www.hochschule-trier.de", 80))
        {
            tcpSocket.sendLine("GET /index.php HTTP/1.1");
            tcpSocket.sendLine("Host: www.hochschule-trier.de");
            tcpSocket.sendLine(""); // Leerzeile ist "Trennzeichen" für
                                    // Web-Server

            String readLine = null;
            while ((readLine = tcpSocket.receiveLine()) != null)
            {
                System.out.println(readLine);
            }
        }
    }
}
