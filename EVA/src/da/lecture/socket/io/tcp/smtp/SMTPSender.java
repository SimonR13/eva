package da.lecture.socket.io.tcp.smtp;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Klasse, deren main-Methode unter Anwendung des SMTP-Protokolls eine E-Mail
 * verschickt. Die Adresse des E-Mail-Servers und die E-Mail-Adresse des
 * Empfängers müssen als Kommandozeilenargumente übergeben werden. Zum
 * tatsächlichen Versenden der E-Mail wird die Java-Mail-API verwendet
 * (http://www.oracle.com/technetwork/java/javamail/index.html).
 */
public class SMTPSender
{
    /**
     * Hauptprogramm. Als Kommandozeilenargumente müssen die Adresse des
     * E-Mail-Servers und die E-Mail-Adresse des Empfängers angegeben weden.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws MessagingException
     *             Wird ausgelöst, wenn es bei der Kommunikation mit dem
     *             SMTP-Server zu einem Ein-/Ausgabefehler gekommen ist.
     */
    public static void main(final String[] args) throws MessagingException
    {
        final Properties props = new Properties();
        props.put("mail.smtp.host", args[0]);

        final Session session = Session.getInstance(props);
        final MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("Otto@otto-waalkes.com"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(args[1]));
        msg.setText("Der Ostfriese trinkt wenig aber oft und dann viel!");

        Transport.send(msg);
    }
}
