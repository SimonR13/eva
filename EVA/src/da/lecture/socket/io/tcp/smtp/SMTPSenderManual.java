package da.lecture.socket.io.tcp.smtp;

import java.io.IOException;

import da.lecture.socket.io.tcp.TCPSocket;

/**
 * Klasse, deren main-Methode unter Anwendung des SMTP-Protokolls eine E-Mail
 * verschickt. Die Adresse des E-Mail-Servers und die E-Mail-Adresse des
 * Empfängers müssen als Kommandozeilenargumente übergeben werden.
 */
public class SMTPSenderManual
{
    /**
     * Hauptprogramm. Als Kommandozeilenargumente müssen die Adresse des
     * E-Mail-Servers und die E-Mail-Adresse des Empfängers angegeben weden.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link TCPSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final TCPSocket tcpSocket = new TCPSocket(args[0], 25))
        {
            tcpSocket.sendLine("HELO egal.hochschule-trier.de");
            tcpSocket.sendLine("MAIL FROM: <Otto@otto-waalkes.com>");
            tcpSocket.sendLine("RCPT TO: <" + args[1] + ">");
            tcpSocket.sendLine("DATA");
            tcpSocket.sendLine("Der Ostfriese trinkt wenig aber oft und dann viel!");
            tcpSocket.sendLine(".");
            tcpSocket.sendLine("QUIT");

            String readLine = null;
            while ((readLine = tcpSocket.receiveLine()) != null)
            {
                System.out.println(readLine);
            }
        }
    }
}
