package da.lecture.socket.io.tcp.counter.delaying;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import da.lecture.socket.io.tcp.counter.CounterServer;

/**
 * TCP-Server, der einen int-Zähler verwaltet und die Befehle "increment" und
 * "reset" versteht. Auf jeden Befehl wird der Zählerstand nach dem Befehl
 * geantwortet. Vor dem Antworten schindet der Server ein wenig Zeit um dem
 * Client vorzugaukeln, dass es sich um langandauernde Operationen handelt. Für
 * die Abarbeitung der Befehle wird pro Verbindung ein eigener Thread verwendet.
 */
public class DynamicParallelCounterServer
{
    /** Port, auf dem der TCP-Server auf Befehle lauscht. */
    public static final int DEFAULT_PORT = 1250;

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Verbindungsannahme über das
     *             verwendete {@link ServerSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final SynchronizedCounter counter = new SynchronizedCounter();
        try (final ServerSocket sSocket = new ServerSocket(CounterServer.DEFAULT_PORT))
        {
            while (true)
            {
                final Socket socketToClient = sSocket.accept();
                final CounterServerRunnable runnable = new CounterServerRunnable(socketToClient, counter);
                final Thread workerThread = new Thread(runnable);
                workerThread.start();
            }
        }
    }
}
