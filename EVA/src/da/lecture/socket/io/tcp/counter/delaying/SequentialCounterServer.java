package da.lecture.socket.io.tcp.counter.delaying;

import java.io.IOException;
import java.net.ServerSocket;

import da.lecture.socket.io.tcp.TCPSocket;
import da.lecture.socket.io.tcp.counter.CounterServer;

/**
 * TCP-Server, der einen int-Zähler verwaltet und die Befehle "increment" und
 * "reset" versteht. Auf jeden Befehl wird der Zählerstand nach dem Befehl
 * geantwortet. Vor dem Antworten schindet der Server ein wenig Zeit um dem
 * Client vorzugaukeln, dass es sich um langandauernde Operationen handelt. Die
 * Abarbeitung der Befehle erfolgt duch einen einzelnen Thread.
 */
public class SequentialCounterServer
{
    /** Port, auf dem der TCP-Server auf Befehle lauscht. */
    public static final int DEFAULT_PORT = 1250;

    /**
     * Hilfsmethode, die den aufrufenden Thread für die übergebene Anzahl
     * Millisekunden schlafen legt.
     * 
     * @param millisToDelay
     *            Anzahl Millisekunden die geschlafen werden soll.
     */
    private static void delay(final long millisToDelay)
    {
        try
        {
            Thread.sleep(millisToDelay);
        }
        catch (final InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Hilfsmethode, die einen übergebenen Befehl auf dem übergebenen Zähler
     * ausführt und den neuen Zählerstand als Antwort zurückliefert.
     * 
     * @param request
     *            Auszuführender Befehl.
     * @param counter
     *            Zähler, auf dem der Befehl ausgeführt werden soll.
     * @return Zählerstand nach Ausführung des Befehls.
     */
    private static int processRequestAndReturnAnswer(final String request, final SynchronizedCounter counter)
    {
        if (request.equals("increment"))
        {
            return counter.increment();
        }
        else if (request.equals("reset"))
        {
            return counter.reset();
        }

        throw new RuntimeException("Ungültige Anfrage " + request);
    }

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link TCPSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final SynchronizedCounter counter = new SynchronizedCounter();
        try (final ServerSocket sSocket = new ServerSocket(CounterServer.DEFAULT_PORT))
        {
            while (true)
            {
                try (final TCPSocket tcpSocket = new TCPSocket(sSocket.accept()))
                {
                    String request = null;
                    while ((request = tcpSocket.receiveLine()) != null)
                    {
                        final int answer = SequentialCounterServer.processRequestAndReturnAnswer(request, counter);
                        SequentialCounterServer.delay(6000);
                        tcpSocket.sendLine(String.valueOf(answer));
                    }
                }
            }
        }
    }
}
