package da.lecture.socket.io.tcp.counter;

import java.io.IOException;

import da.lecture.socket.io.tcp.TCPSocket;
import da.lecture.socket.io.udp.UDPSocket;

/**
 * TCP-Client, der verschiedene Befehle an einen {@link CounterServer} schickt.
 */
public class CounterClient
{
    /**
     * Hauptprogramm. Als Kommandozeilenargumente müssen die Adresse des
     * {@link CounterServer}s übergeben werden sowie die Anzahl an zu
     * versendenden "increment"-Befehlen.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link UDPSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final TCPSocket tcpSocket = new TCPSocket(args[0], CounterServer.DEFAULT_PORT))
        {
            tcpSocket.sendLine("reset");
            System.out.println("Antwort: " + tcpSocket.receiveLine());

            final int incrementCount = Integer.parseInt(args[1]);
            for (int i = 0; i < incrementCount; i++)
            {
                tcpSocket.sendLine("increment");
                System.out.println("Antwort: " + tcpSocket.receiveLine());
            }
        } // Schließen von tcpSocket automatisch durch try-with-resources
    }
}
