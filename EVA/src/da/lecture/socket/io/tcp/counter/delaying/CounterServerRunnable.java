package da.lecture.socket.io.tcp.counter.delaying;

import java.io.IOException;
import java.net.Socket;

import da.lecture.socket.io.tcp.TCPSocket;

/**
 * Runnable-Klasse, die die Befehle "increment" und "reset" von einem über den
 * Konstruktor übergebenen Socket liest und diese auf einen ebenfalls über den
 * Konstruktor übergebenen {@link SynchronizedCounter} anwendet. Auf jeden
 * Befehl wird der Zählerstand nach dem Befehl geantwortet. Vor dem Antworten
 * schindet die Klasse ein wenig Zeit um dem Client vorzugaukeln, dass es sich
 * um langandauernde Operationen handelt.
 */
public class CounterServerRunnable implements Runnable
{
    /** {@link Socket}, von dem die Befehle gelesen werden sollen. */
    private final Socket socket;

    /**
     * Zähler, auf den die Befehle angewendet und dessen Zählerstände
     * geantwortet werden sollen.
     */
    private final SynchronizedCounter counter;

    /**
     * Initialisiert eine neue CounterServerDelayingRunnable-Instanz mit den
     * übergebenen Argumenten.
     * 
     * @param socket
     *            {@link Socket}, von dem die Befehle gelesen werden sollen.
     * @param counter
     *            Zähler, auf den die Befehle angewendet und dessen Zählerstände
     *            geantwortet werden sollen.
     */
    public CounterServerRunnable(final Socket socket, final SynchronizedCounter counter)
    {
        this.socket = socket;
        this.counter = counter;
    }

    @Override
    public void run()
    {
        try (final TCPSocket tcpSocket = new TCPSocket(this.socket))
        {
            String request = null;
            while ((request = tcpSocket.receiveLine()) != null)
            {
                final int answer = this.processRequestAndReturnAnswer(request);
                this.delay(6000);
                tcpSocket.sendLine(String.valueOf(answer));
            }

        }
        catch (final IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    /**
     * Hilfsmethode, die einen übergebenen Befehl auf dem Zähler im Attribut
     * {@link CounterServerRunnable#counter} ausführt und den neuen Zählerstand
     * als Antwort zurückliefert.
     * 
     * @param request
     *            Auszuführender Befehl.
     * @return Zählerstand nach Ausführung des Befehls.
     */
    private int processRequestAndReturnAnswer(final String request)
    {
        if (request.equals("increment"))
        {
            return this.counter.increment();
        }
        else if (request.equals("reset"))
        {
            return this.counter.reset();
        }

        throw new RuntimeException("Ungültige Anfrage " + request);
    }

    /**
     * Hilfsmethode, die den aufrufenden Thread für die übergebene Anzahl
     * Millisekunden schlafen legt.
     * 
     * @param millisToDelay
     *            Anzahl Millisekunden die geschlafen werden soll.
     */
    private void delay(final long millisToDelay)
    {
        try
        {
            Thread.sleep(millisToDelay);
        }
        catch (final InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
