package da.lecture.socket.io.tcp.counter.delaying;

import java.io.IOException;
import java.net.ServerSocket;

import da.lecture.socket.io.tcp.TCPSocket;
import da.lecture.socket.io.tcp.counter.CounterServer;

/**
 * TCP-Server, der einen int-Zähler verwaltet und die Befehle "increment" und
 * "reset" versteht. Auf jeden Befehl wird der Zählerstand nach dem Befehl
 * geantwortet. Vor dem Antworten schindet der Server ein wenig Zeit um dem
 * Client vorzugaukeln, dass es sich um langandauernde Operationen handelt. Für
 * die Abarbeitung der Befehle wird eine konstante Anzahl an Threads verwendet,
 * die sich durch die Konstante {@link StaticParallelCounterServer#WORKER_COUNT}
 * bestimmt.
 */
public class StaticParallelCounterServer
{
    /** Port, auf dem der TCP-Server auf Befehle lauscht. */
    public static final int DEFAULT_PORT = 1250;

    /**
     * Anzahl an Threads, die zur Abarbeitung von Verbindungen verwendet werden.
     */
    public static final int WORKER_COUNT = 4;

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Verbindungsannahme über das
     *             verwendete {@link ServerSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final SynchronizedCounter counter = new SynchronizedCounter();
        final ServerSocket sSocket = new ServerSocket(CounterServer.DEFAULT_PORT);

        for (int i = 0; i < StaticParallelCounterServer.WORKER_COUNT; i++)
        {
            final WorkerThread worker = new WorkerThread(sSocket, counter);
            worker.start();
        }
    }

    //
    //

    /**
     * Thread, der Verbindungen auf dem im Konstruktor übergebenen
     * {@link ServerSocket} annimmt und die über diese Verbindungen empfagenen
     * Befehle auf den ebenfalls über den Konstruktor übergebenen Zähler
     * anwendet.
     */
    private static class WorkerThread extends Thread
    {
        /** {@link ServerSocket} zur Annahme von Verbindungen. */
        private final ServerSocket sSocket;

        /** Zähler, auf den die empfagenen Befehle angewendet werden sollen. */
        private final SynchronizedCounter counter;

        /**
         * Initialisiert eine neue WorkerThread-Instanz mit den übergebenen
         * Argumenten.
         * 
         * @param sSocket
         *            {@link ServerSocket} zur Annahme von Verbindungen.
         * @param counter
         *            Zähler, auf den die empfagenen Befehle angewendet werden
         *            sollen.
         */
        public WorkerThread(final ServerSocket sSocket, final SynchronizedCounter counter)
        {
            this.sSocket = sSocket;
            this.counter = counter;
        }

        @Override
        public void run()
        {
            while (true)
            {
                try (final TCPSocket tcpSocket = new TCPSocket(this.sSocket.accept()))
                {
                    String request = null;
                    while ((request = tcpSocket.receiveLine()) != null)
                    {
                        final int answer = this.processRequestAndReturnAnswer(request);
                        this.delay(6000);
                        tcpSocket.sendLine(String.valueOf(answer));
                    }

                }
                catch (final IOException ioe)
                {
                    ioe.printStackTrace();
                }
            }
        }

        /**
         * Hilfsmethode, die einen übergebenen Befehl auf dem Zähler im Attribut
         * {@link CounterServerRunnable#counter} ausführt und den neuen
         * Zählerstand als Antwort zurückliefert.
         * 
         * @param request
         *            Auszuführender Befehl.
         * @return Zählerstand nach Ausführung des Befehls.
         */
        private int processRequestAndReturnAnswer(final String request)
        {
            if (request.equals("increment"))
            {
                return this.counter.increment();
            }
            else if (request.equals("reset"))
            {
                return this.counter.reset();
            }

            throw new RuntimeException("Ungültige Anfrage " + request);
        }

        /**
         * Hilfsmethode, die den aufrufenden Thread für die übergebene Anzahl
         * Millisekunden schlafen legt.
         * 
         * @param millisToDelay
         *            Anzahl Millisekunden die geschlafen werden soll.
         */
        private void delay(final long millisToDelay)
        {
            try
            {
                Thread.sleep(millisToDelay);
            }
            catch (final InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
