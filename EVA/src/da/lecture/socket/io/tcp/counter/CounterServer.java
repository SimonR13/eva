package da.lecture.socket.io.tcp.counter;

import java.io.IOException;
import java.net.ServerSocket;

import da.lecture.socket.io.tcp.TCPSocket;

/**
 * TCP-Server, der einen int-Zähler verwaltet und die Befehle "increment" und
 * "reset" versteht. Auf jeden Befehl wird der Zählerstand nach dem Befehl
 * geantwortet.
 */
public class CounterServer
{
    /** Port, auf dem der TCP-Server auf Befehle lauscht. */
    public static final int DEFAULT_PORT = 1250;

    /** Verwalteter int-Zähler. */
    private static int counter;

    /**
     * Hilfsmethode, die einen übergebenen Befehl ausführt und den neuen
     * Zählerstand als Antwort zurückliefert.
     * 
     * @param request
     *            Auszuführender Befehl.
     * @return Zählerstand nach Ausführung des Befehls.
     */
    private static String processRequestAndReturnAnswer(final String request)
    {
        if (request.equals("increment"))
        {
            CounterServer.counter++;
        }
        else if (request.equals("reset"))
        {
            CounterServer.counter = 0;
        }

        return String.valueOf(CounterServer.counter);
    }

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link TCPSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final ServerSocket sSocket = new ServerSocket(CounterServer.DEFAULT_PORT))
        {
            while (true)
            {
                try (final TCPSocket tcpSocket = new TCPSocket(sSocket.accept()))
                {
                    String request = null;
                    while ((request = tcpSocket.receiveLine()) != null)
                    { // Blockiert bis Empfang
                        final String answer = CounterServer.processRequestAndReturnAnswer(request);
                        tcpSocket.sendLine(answer);
                    }
                } // Schließen von tcpSocket automatisch durch
                  // try-with-resources
            }
        }
    }
}
