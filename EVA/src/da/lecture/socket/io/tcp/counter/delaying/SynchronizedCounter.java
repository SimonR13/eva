package da.lecture.socket.io.tcp.counter.delaying;

/**
 * Klasse zum synchronisierten Zurücksetzen und Inkrementieren eines
 * int-Zählwertes.
 */
public class SynchronizedCounter
{
    /** Gekapselter int-Zählwert. */
    private int counter;

    /**
     * Setzt den int-Zählwert auf 0 und liefern dessen Wert zurück.
     * 
     * @return 0
     */
    public synchronized int reset()
    {
        this.counter = 0;
        return this.counter;
    }

    /**
     * Erhöht den aktuellen int-Zählwert um 1 und liefert ihn zurück.
     * 
     * @return Aktueller int-Zählwert plus 1
     */
    public synchronized int increment()
    {
        this.counter++;
        return this.counter;
    }
}
