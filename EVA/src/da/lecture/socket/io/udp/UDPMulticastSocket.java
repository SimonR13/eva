package da.lecture.socket.io.udp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * Erweiterung von {@link UDPSocket}, die das Empfangen von Zeichenketten aus
 * einer UDP-Multicast-Gruppe heraus ermöglicht.
 */
public class UDPMulticastSocket extends UDPSocket
{
    /**
     * Initialisiert eine neue UDPMulticastSocket-Instanz, wobei zum Empfangen
     * und Versenden ein UDP-Socket mit dem übergebenen Port verwendet wird.
     * 
     * @param port
     *            Port, auf dem das UDP-Socket geöffnet werden soll.
     * @throws IOException
     *             Wird ausgelöst, wenn beim Erzeugen des UDP-Sockets ein
     *             Problem aufgetreten ist.
     */
    public UDPMulticastSocket(final int port) throws IOException
    {
        super(new MulticastSocket(port));
    }

    /**
     * Tritt der UDP-Muticast-Gruppe mit der übergebenen Gruppenadresse bei.
     * 
     * @param groupAddress
     *            Gruppenadresse.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Beitritt zu einem
     *             Ein-/Ausgabefehler gekommen ist.
     */
    public void join(final InetAddress groupAddress) throws IOException
    {
        ((MulticastSocket) super.socket).joinGroup(groupAddress);
    }

    /**
     * Verlässt die UDP-Muticast-Gruppe mit der übergebenen Gruppenadresse.
     * 
     * @param groupAddress
     *            Gruppenadresse.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Verlassen zu einem
     *             Ein-/Ausgabefehler gekommen ist.
     */
    public void leave(final InetAddress groupAddress) throws IOException
    {
        ((MulticastSocket) super.socket).leaveGroup(groupAddress);
    }
}
