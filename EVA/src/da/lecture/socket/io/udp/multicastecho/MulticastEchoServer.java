package da.lecture.socket.io.udp.multicastecho;

import java.io.IOException;
import java.net.InetAddress;

import da.lecture.socket.io.udp.UDPMulticastSocket;

/**
 * UDP-Server, der einer Multicast-Gruppe beitritt und die über UDP-Datagramme
 * empfangenen Zeichenketten unverändert wieder zurücksendet. Der Empfang der
 * Nachricht "exit" führt dazu, dass sich der Server selbst beendet.
 */
public class MulticastEchoServer
{
    /** Port, auf dem der UDP-Server auf Befehle lauscht. */
    public static final int DEFAULT_PORT = 1250;

    /**
     * Hauptprogramm. Als Kommandozeilenargument muss die Adresse der
     * Multicast-Gruppe angegeben werden.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link UDPMulticastSocket} zu einem
     *             Ein-/Ausgabefehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final InetAddress mcgroupAddress = InetAddress.getByName(args[0]);
        try (final UDPMulticastSocket udpMcSocket = new UDPMulticastSocket(MulticastEchoServer.DEFAULT_PORT))
        {
            udpMcSocket.join(mcgroupAddress); // Beitritt zur übergebenen
                                              // Multicast-Gruppe

            while (true)
            {
                final String request = udpMcSocket.receive(); // Blockiert bis
                                                              // Empfang
                System.out.println(udpMcSocket.getSenderOfLastReceivedString() + " -> " + request);
                udpMcSocket.reply(request);

                if (request.equals("exit"))
                {
                    break;
                }
            }

            udpMcSocket.leave(mcgroupAddress); // Austritt aus übergebener
                                               // Multicast-Gruppe
        }
    }
}
