package da.lecture.socket.io.udp.multicastecho;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

import da.lecture.socket.io.udp.UDPSocket;

/**
 * UDP-Client, der verschiedene Nachrichten an eine Multicast-Gruppe schickt und
 * die Antworten auf die Nachrichten in der Konsole ausgibt.
 */
public class MulticastEchoClient
{
    /**
     * Anzahl an Millisekunden, die auf eine Antwort vom
     * {@link MulticastEchoServer}n gewartet werden soll.
     */
    private static final int TIMEOUT = 2000;

    /**
     * Hauptprogramm. Als Kommandozeilenargumente müssen die Adresse der
     * Multicast-Gruppe übergeben werden sowie die zu versendenden Nachrichten.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link UDPSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final UDPSocket udpSocket = new UDPSocket())
        {
            udpSocket.setTimeout(MulticastEchoClient.TIMEOUT);

            final InetAddress mcgroupAddress = InetAddress.getByName(args[0]);
            for (int i = 1; i < args.length; i++)
            {
                udpSocket.send(args[i], mcgroupAddress, MulticastEchoServer.DEFAULT_PORT);

                try
                {
                    while (true)
                    {
                        final String reply = udpSocket.receive();
                        System.out.println(udpSocket.getSenderOfLastReceivedString() + " -> " + reply);
                    }
                }
                catch (final SocketTimeoutException ste)
                {
                    // Keine weiteren Antworten eingetroffen
                }
            }
        }
    }
}
