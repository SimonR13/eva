package da.lecture.socket.io.udp.daytime;

import java.io.IOException;
import java.net.InetAddress;

import da.lecture.socket.io.udp.UDPSocket;

/**
 * Einfaches UDP-Beispiel, welches das aktuelle Datum und die aktuelle Uhrzeit
 * unter Verwendung des Daytime-Protokolls (siehe auch
 * http://de.wikipedia.org/wiki/Daytime) von einem Server abruft.
 */
public class DaytimeQuerier
{
    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommenadozeilenargumente (Werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation mit dem Server
     *             zu einem Ein-/Ausgabefehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final UDPSocket sock = new UDPSocket())
        {
            final InetAddress serverAddress = InetAddress.getByName("zeit.fu-berlin.de");

            sock.send("KnockKnock", serverAddress, 13);
            System.out.println("Datum/Zeit: " + sock.receive());
        }
    }
}
