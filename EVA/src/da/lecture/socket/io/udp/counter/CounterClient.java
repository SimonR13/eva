package da.lecture.socket.io.udp.counter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

import da.lecture.socket.io.udp.UDPSocket;

/**
 * UDP-Client, der verschiedene Befehle an einen {@link CounterServer} schickt.
 */
public class CounterClient
{
    /**
     * Anzahl an Millisekunden, die auf eine Antwort vom {@link CounterServer}
     * gewartet werden soll.
     */
    private static final int TIMEOUT = 2000;

    /**
     * Hauptprogramm. Als Kommandozeilenargumente müssen die Adresse des
     * {@link CounterServer}s übergeben werden sowie die Anzahl an zu
     * versendenden "increment"-Befehlen.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link UDPSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final UDPSocket udpSocket = new UDPSocket())
        {
            udpSocket.setTimeout(CounterClient.TIMEOUT);

            final InetAddress serverAddress = InetAddress.getByName(args[0]);
            udpSocket.send("reset", serverAddress, CounterServer.DEFAULT_PORT);

            final int incrementCount = Integer.parseInt(args[1]);
            for (int i = 0; i < incrementCount; i++)
            {
                udpSocket.send("increment", serverAddress, CounterServer.DEFAULT_PORT);
            }

            for (int i = 0; i < (incrementCount + 1); i++)
            {
                System.out.println("Antwort: " + udpSocket.receive());
            }
        }
        catch (final SocketTimeoutException ste)
        {
            System.out.println("ACHTUNG: Vermutlich Nachrichtenverlust aufgetreten!");
        }
    }
}
