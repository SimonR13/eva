package da.lecture.socket.io.udp.counter;

import java.io.IOException;

import da.lecture.socket.io.udp.UDPSocket;

/**
 * UDP-Server, der einen int-Zähler verwaltet und die Befehle "increment" und
 * "reset" versteht. Auf jeden Befehl wird der Zählerstand nach dem Befehl
 * geantwortet.
 */
public class CounterServer
{
    /** Port, auf dem der UDP-Server auf Befehle lauscht. */
    public static final int DEFAULT_PORT = 1250;

    /** Verwalteter int-Zähler. */
    private static int counter;

    /**
     * Hilfsmethode, die einen übergebenen Befehl ausführt und den neuen
     * Zählerstand als Antwort zurückliefert.
     * 
     * @param request
     *            Auszuführender Befehl.
     * @return Zählerstand nach Ausführung des Befehls.
     */
    private static String processRequestAndReturnAnswer(final String request)
    {
        if (request.equals("increment"))
        {
            CounterServer.counter++;
        }
        else if (request.equals("reset"))
        {
            CounterServer.counter = 0;
        }

        return String.valueOf(CounterServer.counter);
    }

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link UDPSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final UDPSocket udpSocket = new UDPSocket(CounterServer.DEFAULT_PORT))
        {
            while (true)
            {
                final String request = udpSocket.receive(); // Blockiert bis
                                                            // Empfang
                final String answer = CounterServer.processRequestAndReturnAnswer(request);
                udpSocket.reply(answer);
            }
        }
    }
}
