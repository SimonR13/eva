package da.lecture.socket.io.udp.simpleexample;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.Charset;

/**
 * Einfacher UDP-Server zur Veranschaulichung des prinzipiellen Aufbaus einer
 * UDP-Serveranwendung. Der Server horcht auf Port 1250 auf ankommende
 * Datagramme, deren Inhalt er als UTF-8-kodierte Zeichenektten auffasst. Vor
 * diese Zeichenektte wird die Zeichenkette "Selber " gehängt und das Ergebnis
 * wird UTF-8-kodiert an den Client zurückgesendet.
 */
public class SimpleServer
{
    /**
     * Hilfsmethode zur Abarbeitung einer Anfrage, die als Datagramm vom
     * übergebenen {@link DatagramSocket} gelesen wird.
     * 
     * @param socket
     *            {@link DatagramSocket}, von dem die Anfrage eines Clients
     *            gelesen und über das die Antwort an den anfragenden Client
     *            gesendet werden soll.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link DatagramSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    private static void processNextRequest(final DatagramSocket socket) throws IOException
    {
        final byte[] inBuff = new byte[128];
        final DatagramPacket inDg = new DatagramPacket(inBuff, inBuff.length);
        socket.receive(inDg); // Blockiert, bis Datagramm empfagen wurde
        final String request = new String(inBuff, 0, inDg.getLength(), Charset.forName("UTF-8"));

        final byte[] response = ("Selber " + request).getBytes(Charset.forName("UTF-8"));
        final DatagramPacket dg = new DatagramPacket(response, response.length, inDg.getAddress(), inDg.getPort());
        socket.send(dg);
    }

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link DatagramSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final DatagramSocket socket = new DatagramSocket(1250))
        {
            while (true)
            {
                SimpleServer.processNextRequest(socket);
            }
        }
    }
}
