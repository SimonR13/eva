package da.lecture.socket.io.udp.simpleexample;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.Charset;

/**
 * Einfacher UDP-Client zur Veranschaulichung des prinzipiellen Aufbaus einer
 * UDP-Clientanwendung. Dazu wird die Zeichenkette "Hallo" UTF-8-kodiert in ein
 * Datagramm gepackt und an einen UDP-Server verschickt, der auf Port 1250 unter
 * einer per Kommandozeile zu übergenden Adresse lauscht. Danach wird eine
 * Antwort des Servers abgewartet und diese wird auf der Konsole ausgegeben.
 */
public class SimpleClient
{
    /**
     * Hauptprogramm. Als Kommandozeilenargument muss die Adresse (IP-Adresse
     * oder DNS-Name) des Servers übergeben werden
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link DatagramSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final DatagramSocket socket = new DatagramSocket())
        { // bel. freie Portnummer
            final byte[] request = "Hallo!".getBytes(Charset.forName("UTF-8"));
            final int port = 1250;
            final InetAddress ipAddr = InetAddress.getByName(args[0]); // IP-Adresse
                                                                       // od.
                                                                       // DNS-Name
            final DatagramPacket dg = new DatagramPacket(request, request.length, ipAddr, port);
            socket.send(dg);

            final byte[] inBuff = new byte[128];
            final DatagramPacket inDg = new DatagramPacket(inBuff, inBuff.length);
            socket.receive(inDg); // Blockiert, bis Datagramm empfagen wurde
            final String answer = new String(inBuff, 0, inDg.getLength(), Charset.forName("UTF-8"));
            System.out.println(answer);
        }
    }
}
