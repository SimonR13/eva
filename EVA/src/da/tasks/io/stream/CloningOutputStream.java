package da.tasks.io.stream;

import java.io.IOException;
import java.io.OutputStream;

/*Schreiben Sie eine Ausgabeklasse zur Vervielfältigung der Ausgabe. Diese Klasse soll aus
 OutputStream abgeleitet sein und im Konstruktor beliebig viele OutputStream-Objekte
 akzeptieren (als Feld oder als Varargs). Jedes Schreiben soll auf alle Ausgabeströme
 vervielfältigt werden. Achten Sie genau darauf, welche Methoden der abstrakten Klas-
 se OutputStream überschrieben werden müssen. Probieren Sie Ihre neue Klasse aus.
 */

public class CloningOutputStream extends OutputStream
{

    private OutputStream[] outputStreams;

    public CloningOutputStream(OutputStream[] outputStreams)
    {
        this.outputStreams = outputStreams;
    }

    @Override
    public void write(int b) throws IOException
    {
        for (OutputStream os : outputStreams)
        {
            os.write(b);
        }
    }
}
