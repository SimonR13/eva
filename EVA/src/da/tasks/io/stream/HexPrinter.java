package da.tasks.io.stream;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/*Schreiben Sie ein Programm, mit dem Sie den Inhalt einer Datei in Hexadezimaldarstellung
 auf den Bildschirm ausgeben. Geben Sie immer 16 Bytes pro Zeile aus und geben Sie zu Be-
 ginn jeder Zeile die Nummer des ersten Bytes der Zeile ebenfalls in Hexadezimaldarstellung
 an. Die Zählung beginnt bei 0. Der Name der auszugebenden Datei soll als Kommandozei-
 lenargument angegeben werden. Verwenden Sie zur Ausgabe die in der vorherigen Aufgabe
 betrachtete Methode PrintStream.format(String format, Object... args).
 */

public class HexPrinter
{

    public static void main(String[] args)
    {
        String file = "";
        if (args.length > 0 && args[0].length() > 0)
        {
            file = args[0];

            try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(file)))
            {
                System.out.println("ByteNr | Bytes");
                System.out.println("---------------------------------------------------------");
                int nummer = 0;
                int data = 0;
                System.out.print(String.format("0x%04X", nummer) + " | ");
                while ((data = is.read()) != -1)
                {
                    if (data < 16)
                    {
                        System.out.print("0");
                    }
                    System.out.print(Integer.toHexString(data).toUpperCase() + " ");
                    nummer++;

                    if (((nummer) % 16) == 0)
                    {
                        System.out.println();
                        System.out.print(String.format("0x%04X", nummer) + " | ");
                    }
                }
                System.out.println();
                System.out.println();

            }
            catch (FileNotFoundException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else
        {
            System.err.println("Kein Argument uebergen...");
        }
    }

}
