package da.tasks.io.stream.serialization;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import da.lecture.io.stream.serialization.Point;

public class RepeatedSerialization
{
    static class NeighbourPoint extends Point
    {
        /**
         * 
         */
        private static final long serialVersionUID = -7151859379112987579L;

        private NeighbourPoint neighbour;

        public NeighbourPoint(int x, int y)
        {
            super(x, y);
        }

        public NeighbourPoint(int x, int y, NeighbourPoint neighbour)
        {
            super(x, y);
            this.neighbour = neighbour;
        }

        public NeighbourPoint getNeighbour()
        {
            return this.neighbour;
        }

        public void setNeighbour(NeighbourPoint neighbour)
        {
            this.neighbour = neighbour;
        }

    }

    private static NeighbourPoint a;

    private static NeighbourPoint b;

    private static NeighbourPoint aStrich;

    private static NeighbourPoint bStrich;

    private static String file = "TestRS.txt";

    public static void main(String[] args)
    {
        // vorarbeit
        b = new NeighbourPoint(23, 42);
        a = new NeighbourPoint(5, 21, b);

        // Schreiben der Testdatei mit den 2 Objekten
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file))))
        {
            // Objekt a (mit referenz auf b) serialisieren
            oos.writeObject(a);
            // Objekt b (ohne jegliche referenzen) serialisieren
            /* Aufgabe b) 
                b.setX(32);
                b.setY(24);
            */
            oos.writeObject(b);
        }
        catch (Exception e)
        {
            System.err.println("Fehler beim Schreiben der Objekte! Abbruch.\n" + e.getStackTrace());
        }

        // Lesen der Testdatei
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file))))
        {
            System.out.println("Zur Referenz, Punkte/Objekte a und b:\n" + a + "; " + b + ";");
            System.out.println();
            aStrich = (NeighbourPoint) ois.readObject();
            bStrich = (NeighbourPoint) ois.readObject();
            System.out.println("Punkt aStrich: " + aStrich);
            System.out.println("Punkt aStrich.getNeighbour(): " + aStrich.getNeighbour());
            System.out.println("Punkt bStrich: " + bStrich);
            System.out.println("a.equals(aStrich): " + a.equals(aStrich));
            System.out.println("b.equals(bStrich): " + b.equals(bStrich));
            System.out.println("aStrich.getNeighbour().equals(b): " + aStrich.getNeighbour().equals(b));
            System.out.println("aStrich.getNeighbour().equals(bStrich): " + aStrich.getNeighbour().equals(bStrich));
            System.out.println("a == aStrich: " + (a == aStrich));
        }
        catch (Exception e)
        {
            System.err.println("Fehler beim Auslesen der Objekte! Abbruch.\n" + e.getStackTrace());
        }

    }

}
