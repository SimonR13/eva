package da.tasks.io.stream.serialization;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;

import da.lecture.io.stream.serialization.Point;

public class ObjectOutputStreamExamination
{

    private static String file;

    private static final Point PUNKT = new Point(23, 42);

    public static void main(String[] args)
    {
        // Einlesen des Pfades/ Dateinamen
        if (args != null && args.length > 0)
        {
            file = args[0].toString();
        }
        else
        {
            // Fallback Dateiname
            file = "testOOSE.txt";
        }

        /* Begin a) */
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file))))
        {
            // schreiben des punkt in den stream/die datei
            oos.writeObject(PUNKT);
        }
        catch (Exception e)
        {
            System.err.println("Fehler beim Schreiben des Objekts! Abbruch." + e.getLocalizedMessage());
        }
        
        try (InputStreamReader isr = new InputStreamReader(new BufferedInputStream(new FileInputStream(file)), Charset.forName("UTF-8")))
        {
            int data = 0, counter = 0;
            System.out.println("Geschriebene Daten einlesen und ausgeben...");
            while ((data = isr.read()) != -1)
            {
                counter++;
                if (counter >= 16)
                {
                    System.out.println(data);
                    counter = 0;
                }
                else
                {
                    System.out.print(data + " ");
                }
            }
        }
        catch (Exception e)
        {
            System.err.println("Fehler beim Lesen des Objekts! Abbruch.");
        }
        /* End a) */
        System.out.println();
        /* Begin b) */
        // Einlesen des geschriebenen Objekts PUNKT
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file))))
        {
            Point readPUNKT = (Point) ois.readObject();
            if (readPUNKT != null)
            {
                System.out.println("Punkt x-Wert: " + readPUNKT.getX());
                System.out.println("Punkt y-Wert: " + readPUNKT.getY());
                System.out.println("P:" + readPUNKT.toString());
            }
        }
        catch (Exception e)
        {
            System.err.println("Fehler beim Einlesen des Objekts aus der Datei! Abbruch." + e);
        }
        /* End b) */

    }

}
