package da.tasks.io.stream;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;

/*In der Vorlesung wurde die Methode format der Klasse PrintStream nicht behandelt.
 Schauen Sie sich die Verwendung dieser Methode an. Bereiten Sie sich so vor, dass Sie einen
 kurzen Vortrag (ca. 3 Minuten) über die Benutzung dieser Methode halten können und
 dabei die Verwendung dieser Methode an einem ausführbaren Beispiel erläutern können.*/

public class FormatDemonstration
{

    public static void main(String[] args) throws IOException
    {
        try (PrintStream ps = new PrintStream(System.out))
        {
            // Formatierte Ausgabe von 42 nach 0x002A
            ps.format("0x%04X", 42);
            ps.println();
            // Formatierte Ausgabe von 0x42 als 0042
            System.in.read();
            ps.format("%04X", 0X42);
            ps.println();
            // Ausgabe eines Templates mit Platzhaltern
            System.in.read();
            ps.format("%s mit %d %s", "Beispieltext", 2, "Platzhalter");
            ps.println();
            System.in.read();
            // Ausgabe von Datum im "8. April 2014" Format %n ^= Umbruch
            Calendar cal = Calendar.getInstance();
            ps.format("%te. %tB %tY%n", cal, cal, cal);

        }

    }

}
