package da.tasks.io.stream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/*import da.tasks.io.stream.HexPrinter;*/

public class DataStreamExamination
{
    final private static boolean OUT_BOOL = true;

    final private static short OUT_SHORT = 42;

    final private static int OUT_INT = 42;

    final private static long OUT_LONG = 42L;

    final private static String OUT_STRING = "Beispielzeichenkette";

    private static String file = "testFile.txt";

    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        if (args.length > 0)
        {
            file = args[0];
        }

        writeData();

        readData();

        readDataWrong();

    }

    private static void readDataWrong() throws FileNotFoundException, IOException
    {
        try (DataInputStream is = new DataInputStream(new FileInputStream(file)))
        {
            // read utf
            String inString = is.readUTF();
            System.out.println("Gelesener UTF-String: " + inString);
            // read Int
            int inInt = is.readInt();
            System.out.println("Gelesene Integer: " + inInt);
            // read short
            Short inShort = is.readShort();
            System.out.println("Gelesene Short: " + inShort);
            // read boolean
            Boolean inBool = is.readBoolean();
            System.out.println("Gelesene Boolean: " + inBool);
            // read long
            Long inLong = is.readLong();
            System.out.println("Gelesene Long: " + inLong);
            System.in.read();

        }
    }

    private static void readData() throws FileNotFoundException, IOException
    {
        try (DataInputStream is = new DataInputStream(new FileInputStream(file)))
        {
            // read utf
            String inString = is.readUTF();
            System.out.println("Gelesener UTF-String: " + inString);
            // read boolean
            Boolean inBool = is.readBoolean();
            System.out.println("Gelesene Boolean: " + inBool);
            // read short
            Short inShort = is.readShort();
            System.out.println("Gelesene Short: " + inShort);
            // read Int
            int inInt = is.readInt();
            System.out.println("Gelesene Integer: " + inInt);
            // read long
            Long inLong = is.readLong();
            System.out.println("Gelesene Long: " + inLong);
            System.in.read();

        }
    }

    private static void writeData() throws FileNotFoundException, IOException
    {
        // String[] argsFile = new String[]
        // { file };
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file)))
        {
            // writeUTF strings
            dos.writeUTF(OUT_STRING);
            // dos.flush();
            // HexPrinter.main(argsFile);
            // write boolean
            dos.writeBoolean(OUT_BOOL);
            // dos.flush();
            // HexPrinter.main(argsFile);
            // write Short
            dos.writeShort(OUT_SHORT);
            // dos.flush();
            // HexPrinter.main(argsFile);
            // write integer
            dos.writeInt(OUT_INT);
            // dos.flush();
            // HexPrinter.main(argsFile);
            // write long
            dos.writeLong(OUT_LONG);
            // dos.flush();
            // HexPrinter.main(argsFile);
            // writeByte string
            dos.writeBytes(OUT_STRING);
            // dos.flush();
            // HexPrinter.main(argsFile);
            // writeChars String
            dos.writeChars(OUT_STRING);
        }

        /*
         * HexPrinter.main(new String[] { file });
         */
    }

}
