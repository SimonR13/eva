package da.tasks.io.stream;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopyMeasuring
{
    // erwartet als Parameter [fileIn] [fileOut] [int Buffer]
    public static void main(String[] args) throws IOException
    {
        if (args.length < 2)
        {
            System.err.println("Kein Argument fuer Datei uebergeben...");
            System.out.println("Starte neu mit Standardwerten");
            String[] newArgs = new String[]
            { "bytes.txt", "bytesOut.txt", "4" };
            FileCopyMeasuring.main(newArgs);

        }
        else
        {
            final String fileIn = args[0];
            final String fileOut = args[1];
            final int buffer = Integer.parseInt(args[2]);

            Long timeFileStream = -1L, timeBufferedStream = -1L, timeByteFileStream = -1L;
            try
            {
                timeFileStream = measureFileInputStream(fileIn, fileOut);
            }
            catch (Exception e)
            {
                System.err.println("Fehler beim Messen der FileInputStream-Zeit!" + e);
            }

            try
            {
                timeBufferedStream = measureBufferedInputStream(fileIn, fileOut);
            }
            catch (Exception e)
            {
                System.err.println("Fehler beim Messen der BufferedInputStream-Zeit!" + e);
            }

            try
            {
                timeByteFileStream = measureFileInputStreamBytes(fileIn, fileOut, buffer);
            }
            catch (Exception e)
            {
                System.err.println("Fehler beim Messen der FileInputStreamBytes-Zeit!" + e);
            }

            System.out.println("FileStream: " + timeFileStream + " Millisekunden");
            System.out.println("BufferedStream: " + timeBufferedStream + " Millisekunden");
            System.out.println("ByteFileStream: " + timeByteFileStream + " Millisekunden");
        }

    }

    public static Long measureFileInputStream(final String fileIn, final String fileOut) throws FileNotFoundException, IOException
    {
        Long start = -1L, end = -1L;
        try (FileInputStream is = new FileInputStream(fileIn);
        FileOutputStream os = new FileOutputStream(fileOut))
        {
            start = System.currentTimeMillis();
            int data = 0;
            while ((data = is.read()) != -1)
            {
                os.write(data);
            }
            end = System.currentTimeMillis();
        }

        return (end - start);
    }

    public static Long measureBufferedInputStream(final String fileIn, final String fileOut) throws FileNotFoundException, IOException
    {
        Long start = -1L, end = -1L;
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileIn));
        BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(fileOut)))
        {
            start = System.currentTimeMillis();
            int data = 0;
            while ((data = is.read()) != -1)
            {
                os.write(data);
            }
            end = System.currentTimeMillis();
        }

        return (end - start);
    }

    public static Long measureFileInputStreamBytes(final String fileIn, final String fileOut, final int buffer) throws FileNotFoundException, IOException
    {
        Long start = -1L, end = -1L;
        try (FileInputStream is = new FileInputStream(fileIn);
        FileOutputStream os = new FileOutputStream(fileOut))
        {
            start = System.currentTimeMillis();
            byte[] dataBuff = new byte[buffer];
            while ((is.read(dataBuff)) != -1)
            {
                for (int i = 0; i < dataBuff.length - 1; i++)
                {
                    os.write(dataBuff, i, 1);
                }
            }
            end = System.currentTimeMillis();
        }

        return (end - start);
    }

}
